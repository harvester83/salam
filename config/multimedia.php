<?php

return [
    0 => [
        0 => [
            'title' => [
                'az' => 'Hər gün ömürdən gedir',
                'ru' => 'Каждый день уходит...',
                'en' => 'Hər gün ömürdən gedir',
            ],
            'type' => 'video',
            'icon' => 'her_gun_omurden_gedir',
            'source' => 'https://www.youtube.com/embed/R9fZd6XMBNc',
        ],
        1 => [
            'title' => [
                'az' => 'Bəh Bəh',
                'ru' => '---',
                'en' => '---',
            ],
            'type' => 'video',
            'icon' => 'beh_beh',
            'source' => 'https://www.youtube.com/embed/M7FSPtLRvn4',
        ],
        2 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo1',
        ],
        3 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo2',
        ],
        4 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo3',
        ],
        5 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo4',
        ],
        6 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo5',
        ],
        7 => [
            'title' => [
                'az' => '---',
                'ru' => '---',
                'en' => '---',
            ],
            'type' => 'img',
            'icon' => 'photo6',
        ],
        8 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo7',
        ],
    ],

    1 => [
        0 => [
            'title' => [
                'az' => 'Biri vardı, biri yoxdu...',
                'ru' => 'Жили-были...',
                'en' => 'Biri vardı, biri yoxdu...',
            ],
            'type' => 'video',
            'icon' => 'video-02',
            'source' => 'https://www.youtube.com/embed/A4KhKaZbtgU',
        ],
        1 => [
            'title' => [
                'az' => 'Mozalan !"Təşəkkür"',
                'ru' => '---',
                'en' => '---',
            ],
            'type' => 'video',
            'icon' => 'mozalan_teshekkur',
            'source' => 'https://www.youtube.com/embed/rZZhyCflmko',
        ],
        2 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo8',
        ],
        3 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo9',
        ],
        4 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo10',
        ],
        5 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo11',
        ],
        6 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo12',
        ],
        7 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo13',
        ],
        8 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo14',
        ],
    ],

    2 => [
        0 => [
            'title' => [
                'az' => '1:0 Baloğlanın xeyrinə',
                'ru' => '1:0 В пользу Балоглана',
                'en' => '---',
            ],
            'type' => 'video',
            'icon' => '1-0_baloglanın_xeyrine',
            'source' => 'https://www.youtube.com/embed/CnSeOcTniNc',
        ],
        1 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo15',
        ],
        2 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo16',
        ],
        3 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo17',
        ],
        4 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo18',
        ],
        5 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo19',
        ],
        6 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo20',
        ],
        7 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo21',
        ],
    ],

    3 => [
        0 => [
            'title' => [
                'az' => 'Hardasan, ay subaylıq',
                'ru' => '---',
                'en' => '---',
            ],
            'type' => 'video',
            'icon' => 'hardasan_ay_subayliq',
            'source' => 'https://www.youtube.com/embed/8GCyOMtb__4',
        ],
        1 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo22',
        ],
        2 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo23',
        ],
        3 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo24',
        ],
        4 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo25',
        ],
        5 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo26',
        ],
        6 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo27',
        ],
        7 => [
            'title' => [
                'az'  => '---',
                'ru'  => '---',
                'en'  => '---',
            ],
            'type' => 'img',
            'icon' => 'photo28',
        ],
    ],
];