<?php

return [
    'romans' => [
        'name' => [
            'az' => 'ROMANLAR',
            'ru' => 'РОМАНЫ',
            'en' => 'ROMANLAR',
        ],
        'list' => [
            [
                'year' => '1953-1956'   ,
                'name' => [
                    'az' => 'Qış gecəsi',
                    'ru' => 'Зимняя ночь',
                    'en' => 'Qış gecəsi',
                ],
                'files' => [
                    'az' => [
                        'word' => 'romans/qish-gecesi.docx',
                        'pdf' => 'romans/qish-gecesi.pdf',
                        'mobi' => 'romans/qish-gecesi.mobi',
                    ],
                    'ru' => [
                        'word' => 'romans/qish-gecesi.docx',
                        'pdf' => 'romans/qish-gecesi.pdf',
                        'mobi' => 'romans/qish-gecesi.mobi',
                    ],
                    'en' => [
                        'word' => 'romans/qish-gecesi.docx',
                        'pdf' => 'romans/qish-gecesi.pdf',
                        'mobi' => 'romans/qish-gecesi.mobi',
                    ],
                ],
            ],

            [
                'year' => '1953',
                'name' => [
                    'az' => 'Kəndimizdə bir gözəl var',
                    'ru' => 'У нас в селе есть красавица',
                    'en' => 'Kəndimizdə bir gözəl var',
                ],
                'files' => [
                    'az' => [
                        'word' => 'romans/kendimizde-bir-gozel-var.docx',
                        'pdf' => 'romans/kendimizde-bir-gozel-var.pdf',
                        'mobi' => 'romans/kendimizde-bir-gozel-var.mobi',
                    ],
                    'ru' => [
                        'word' => 'romans/kendimizde-bir-gozel-var.docx',
                        'pdf' => 'romans/kendimizde-bir-gozel-var.pdf',
                        'mobi' => 'romans/kendimizde-bir-gozel-var.mobi',
                    ],
                    'en' => [
                        'word' => 'romans/kendimizde-bir-gozel-var.docx',
                        'pdf' => 'romans/kendimizde-bir-gozel-var.pdf',
                        'mobi' => 'romans/kendimizde-bir-gozel-var.mobi',
                    ],
                ],
            ],
        ],
    ],

    'fictions' => [
        'name' => [
            'az' => 'POVESTLƏR',
            'ru' => 'ПОВЕСТИ ',
            'en' => 'ROMANLAR',
        ],

        'list' => [
            [
                'year' => '1958',
                'name' => [
                    'az' => 'Sevdasız aylar',
                    'ru' => 'Месяцы без любви',
                    'en' => 'Sevdasız aylar',
                ],
                'files' => [
                    'az' => [
                        'word' => 'povesti/sevdasiz-aylar.docx',
                        'pdf' => 'povesti/sevdasiz-aylar.pdf',
                        'mobi' => 'povesti/sevdasiz-aylar.mobi',
                    ],
                    'ru' => [
                        'word' => 'povesti/sevdasiz-aylar.docx',
                        'pdf' => 'povesti/sevdasiz-aylar.pdf',
                        'mobi' => 'povesti/sevdasiz-aylar.mobi',
                    ],
                    'en' => [
                        'word' => 'povesti/sevdasiz-aylar.docx',
                        'pdf' => 'povesti/sevdasiz-aylar.pdf',
                        'mobi' => 'povesti/sevdasiz-aylar.mobi',
                    ],
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => '46 bənövşə',
                    'ru' => '46 фиалок',
                    'en' => '46 bənövşə',
                ],
                'files' => [
                    'az' => [
                        'word' => 'povesti/46-benovshe.docx',
                        'pdf' => 'povesti/46-benovshe.pdf',
                        'mobi' => 'povesti/46-benovshe.mobi',
                    ],
                    'ru' => [
                        'word' => 'povesti/46-benovshe.docx',
                        'pdf' => 'povesti/46-benovshe.pdf',
                        'mobi' => 'povesti/46-benovshe.mobi',
                    ],
                    'en' => [
                        'word' => 'povesti/46-benovshe.docx',
                        'pdf' => 'povesti/46-benovshe.pdf',
                        'mobi' => 'povesti/46-benovshe.mobi',
                    ],
                ],
            ],

            [
                'year' => '1949-1953',
                'name' => [
                    'az' => 'Gənclik',
                    'ru' => 'Юность',
                    'en' => 'Gənclik',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1953',
                'name' => [
                    'az' => 'Sarmaşıqlı aynabənd',
                    'ru' => 'Веранда, увитая плющом',
                    'en' => 'Sarmaşıqlı aynabənd',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Su pərisi Marianna',
                    'ru' => 'Русалка Марианна',
                    'en' => 'Su pərisi Marianna',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Başabala-Paşabəla',
                    'ru' => 'Башабала-Пашабала',
                    'en' => 'Başabala-Paşabəla',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Burada insan yaşamışdı',
                    'ru' => 'Здесь жил человек',
                    'en' => 'Burada insan yaşamışdı',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Ömrün təzə illəri',
                    'ru' => 'Юные годы жизни',
                    'en' => 'Ömrün təzə illəri',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Haram pul',
                    'ru' => 'Шальные деньги',
                    'en' => 'Haram pul',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Ulduzlar sayrışanda',
                    'ru' => 'Когда мерцают звезды',
                    'en' => 'Ulduzlar sayrışanda',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Ülkər-Aytəkin',
                    'ru' => 'Улькер-Айтекин',
                    'en' => 'Ülkər-Aytəkin',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Müqəddəs yalan',
                    'ru' => 'Святая ложь',
                    'en' => 'Müqəddəs yalan',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Xəzan yarpaqları',
                    'ru' => 'Осенние листья',
                    'en' => 'Xəzan yarpaqları',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Mahnı dağları aşdı (mənsur poema)',
                    'ru' => 'Песня пересекла горы',
                    'en' => 'Mahnı dağları aşdı (mənsur poema)',
                ],
                'files' => [
                ],
            ],

            [
                'year' => '1958',
                'name' => [
                    'az' => 'Hər gün ömürdən gedir',
                    'ru' => 'И каждый день уходит из жизни',
                    'en' => 'Hər gün ömürdən gedir',
                ],
                'files' => [
                ],
            ],
        ],
    ],


    'stories' => [
        'name' => [
            'az' => 'HEKAYƏLƏR',
            'ru' => 'РАССКАЗЫ',
            'en' => 'STORIES',
        ],
        'list' => [
            'left' => [
                [
                    'year' => '1965',
                    'name' => [
                        'az' => 'Gözbağlıca',
                        'ru' => 'Иллюзия',
                        'en' => 'Gözbağlıca',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/gozbaglica.docx',
                            'pdf' => 'stories/gozbaglica.pdf',
                            'mobi' => 'stories/gozbaglica.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/gozbaglica.docx',
                            'pdf' => 'stories/gozbaglica.pdf',
                            'mobi' => 'stories/gozbaglica.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/gozbaglica.docx',
                            'pdf' => 'stories/gozbaglica.pdf',
                            'mobi' => 'stories/gozbaglica.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1966',
                    'name' => [
                        'az' => 'Bığ',
                        'ru' => 'Усы',
                        'en' => 'Bığ',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/big.docx',
                            'pdf' => 'stories/big.pdf',
                            'mobi' => 'stories/big.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/big.docx',
                            'pdf' => 'stories/big.pdf',
                            'mobi' => 'stories/big.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/big.docx',
                            'pdf' => 'stories/big.pdf',
                            'mobi' => 'stories/big.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Evlənəndə tələsməyin',
                        'ru' => 'Не спешите жениться!',
                        'en' => 'Evlənəndə tələsməyin',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/evlenende-telesmeyin.docx',
                            'pdf' => 'stories/evlenende-telesmeyin.pdf',
                            'mobi' => 'stories/evlenende-telesmeyin.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/evlenende-telesmeyin.docx',
                            'pdf' => 'stories/evlenende-telesmeyin.pdf',
                            'mobi' => 'stories/evlenende-telesmeyin.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/evlenende-telesmeyin.docx',
                            'pdf' => 'stories/evlenende-telesmeyin.pdf',
                            'mobi' => 'stories/evlenende-telesmeyin.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Ər-arvad təhlükəsizlik ongünlüyü',
                        'ru' => 'Декада семейной безопасности',
                        'en' => 'Ər-arvad təhlükəsizlik ongünlüyü',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/er-arvad-tehlukesizlik-onunluyu.docx',
                            'pdf' => 'stories/er-arvad-tehlukesizlik-onunluyu.pdf',
                            'mobi' => 'stories/er-arvad-tehlukesizlik-onunluyu.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/er-arvad-tehlukesizlik-onunluyu.docx',
                            'pdf' => 'stories/er-arvad-tehlukesizlik-onunluyu.pdf',
                            'mobi' => 'stories/er-arvad-tehlukesizlik-onunluyu.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/er-arvad-tehlukesizlik-onunluyu.docx',
                            'pdf' => 'stories/er-arvad-tehlukesizlik-onunluyu.pdf',
                            'mobi' => 'stories/er-arvad-tehlukesizlik-onunluyu.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Telefon',
                        'ru' => 'Телефон',
                        'en' => 'Telefon',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/telefon.docx',
                            'pdf' => 'stories/telefon.pdf',
                            'mobi' => 'stories/telefon.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/telefon.docx',
                            'pdf' => 'stories/telefon.pdf',
                            'mobi' => 'stories/telefon.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/telefon.docx',
                            'pdf' => 'stories/telefon.pdf',
                            'mobi' => 'stories/telefon.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Şlyapalı Mauqli',
                        'ru' => 'Маугли в шляпе',
                        'en' => 'Şlyapalı Mauqli',
                    ],
                    'files' => [
                        'az' => [
                            'word' => 'stories/shlyapali-mauqli.docx',
                            'pdf' => 'stories/shlyapali-mauqli.pdf',
                            'mobi' => 'stories/shlyapali-mauqli.mobi',
                        ],
                        'ru' => [
                            'word' => 'stories/shlyapali-mauqli.docx',
                            'pdf' => 'stories/shlyapali-mauqli.pdf',
                            'mobi' => 'stories/shlyapali-mauqli.mobi',
                        ],
                        'en' => [
                            'word' => 'stories/shlyapali-mauqli.docx',
                            'pdf' => 'stories/shlyapali-mauqli.pdf',
                            'mobi' => 'stories/shlyapali-mauqli.mobi',
                        ],
                    ],
                ],

                [
                    'year' => '1953',
                    'name' => [
                        'az' => 'Sevincin atası',
                        'ru' => 'Отец Севиндж',
                        'en' => 'Sevincin atası',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1954',
                    'name' => [
                        'az' => 'Divarın o tərəfində',
                        'ru' => 'По ту сторону стены',
                        'en' => 'Divarın o tərəfində',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1955',
                    'name' => [
                        'az' => 'Moldavan qızı',
                        'ru' => 'Молдаванка',
                        'en' => 'Moldavan qızı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1956',
                    'name' => [
                        'az' => 'Yarpaqlar pıçıldaşır',
                        'ru' => 'Листья шепчут',
                        'en' => 'Yarpaqlar pıçıldaşır',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1956',
                    'name' => [
                        'az' => 'Bayram axşamı',
                        'ru' => 'раздничный вечер',
                        'en' => 'Bayram axşamı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1959',
                    'name' => [
                        'az' => 'Sənətkardan əsər qalar',
                        'ru' => 'От мастера остается его произведение',
                        'en' => 'Sənətkardan əsər qalar',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1961',
                    'name' => [
                        'az' => 'Mənim rəfiqəm',
                        'ru' => 'Моя подруга',
                        'en' => 'Mənim rəfiqəm',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1961',
                    'name' => [
                        'az' => 'Bulaq başında',
                        'ru' => 'У родника',
                        'en' => 'Bulaq başında',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1962',
                    'name' => [
                        'az' => 'Bacıqızı',
                        'ru' => 'Племянница',
                        'en' => 'Bacıqızı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1962',
                    'name' => [
                        'az' => 'Kağızlardan ürəklərə',
                        'ru' => 'К сердцам со страниц',
                        'en' => 'Kağızlardan ürəklərə',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1963',
                    'name' => [
                        'az' => 'Məhəbbətin ləzzəti zaqsın kəndarına kimidir',
                        'ru' => 'Радость любви – лишь до порога ЗАГСа',
                        'en' => 'Məhəbbətin ləzzəti zaqsın kəndarına kimidir',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1963',
                    'name' => [
                        'az' => 'Yol ayrıcında',
                        'ru' => 'На перепутье',
                        'en' => 'Yol ayrıcında',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1963',
                    'name' => [
                        'az' => '“Qanacaqlı” adam',
                        'ru' => '“Понимающий” человек',
                        'en' => '“Qanacaqlı” adam',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1963',
                    'name' => [
                        'az' => 'Dilənçinin cavabı',
                        'ru' => 'Ответ нищего',
                        'en' => 'Dilənçinin cavabı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1964',
                    'name' => [
                        'az' => 'Milis ürəyi',
                        'ru' => 'Милицейское сердце',
                        'en' => 'Milis ürəyi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1964',
                    'name' => [
                        'az' => 'Eloğlunun fərasəti',
                        'ru' => 'Смекалка Эльоглу',
                        'en' => 'Eloğlunun fərasəti',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1964',
                    'name' => [
                        'az' => 'Qar üstdə qızıl gül',
                        'ru' => 'Роза на снегу',
                        'en' => 'Qar üstdə qızıl gül',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1965',
                    'name' => [
                        'az' => 'Qədiməlinin ulduzları',
                        'ru' => 'Звезды Гадималы',
                        'en' => 'Qədiməlinin ulduzları',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1965',
                    'name' => [
                        'az' => 'Xoruzburaxan Novruzəli',
                        'ru' => 'Новрузали, “пускающий петухов”',
                        'en' => 'Xoruzburaxan Novruzəli',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1965',
                    'name' => [
                        'az' => 'Məsciddən meyxanəyə',
                        'ru' => 'От мечети – к мейхане',
                        'en' => 'Məsciddən meyxanəyə',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1966',
                    'name' => [
                        'az' => 'Toydan sonra nağara',
                        'ru' => 'Молочная сказка',
                        'en' => 'Toydan sonra nağara',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1966',
                    'name' => [
                        'az' => 'Süd nağılı',
                        'ru' => 'Молочная сказка',
                        'en' => 'Süd nağılı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1967',
                    'name' => [
                        'az' => 'Dərəqılınc bağlarında',
                        'ru' => 'Дачи Дерегылынджа',
                        'en' => 'Dərəqılınc bağlarında',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1967',
                    'name' => [
                        'az' => 'Dədə, mənə arvad al',
                        'ru' => 'Папаша, жени меня ',
                        'en' => 'Dədə, mənə arvad al',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1967',
                    'name' => [
                        'az' => 'Q. idi, mən idim, Ş. idi',
                        'ru' => 'Там были – Г., я и Ш.',
                        'en' => 'Q. idi, mən idim, Ş. idi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1968',
                    'name' => [
                        'az' => 'Vay səni, Ənnağı',
                        'ru' => 'Ух ты, Аннаги',
                        'en' => 'Vay səni, Ənnağı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1968',
                    'name' => [
                        'az' => 'Tənha ağac altında',
                        'ru' => 'Под одиноким деревом',
                        'en' => 'Tənha ağac altında',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'O, kağız-qələmlə getdi',
                        'ru' => 'Oн ушел с пером и бумагой в руке',
                        'en' => 'O, kağız-qələmlə getdi',
                    ],
                    'files' => [
                    ],
                ],
                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Cərimə qonaqlığı',
                        'ru' => '“Штрафное” застолье',
                        'en' => 'Cərimə qonaqlığı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Heykəl',
                        'ru' => 'Скульптура',
                        'en' => 'Heykəl',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Qırmızı işıq',
                        'ru' => 'Красный свет',
                        'en' => 'Qırmızı işıq',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Futbol xırmanda olacaq',
                        'ru' => 'Футбольный матч состоится на пашне',
                        'en' => 'Futbol xırmanda olacaq',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Quşatan',
                        'ru' => 'Рогатка',
                        'en' => 'Quşatan',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Əlvan lampalar',
                        'ru' => 'Яркие лампы',
                        'en' => 'Əlvan lampalar',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Ata dözümü',
                        'ru' => 'Отцовское терпение',
                        'en' => 'Ata dözümü',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Vəsiyyət',
                        'ru' => 'Завещание',
                        'en' => 'Vəsiyyət',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Məsləhət',
                        'ru' => 'Совет',
                        'en' => 'Məsləhət',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Qəlyan əmi',
                        'ru' => 'Дедушка Кальян',
                        'en' => 'Qəlyan əmi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Armud',
                        'ru' => 'Груша',
                        'en' => 'Armud',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Eynəkli qarı',
                        'ru' => 'Старушка в очках',
                        'en' => 'Eynəkli qarı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Iki günün sevgisi',
                        'ru' => 'Любовь двух дней',
                        'en' => 'Iki günün sevgisi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Mən və mən',
                        'ru' => 'Я и я',
                        'en' => 'Mən və mən',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Kəmşirin',
                        'ru' => 'Полусладкое',
                        'en' => 'Kəmşirin',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Nəsihət',
                        'ru' => 'Наставление',
                        'en' => 'Nəsihət',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Şikayətçi',
                        'ru' => 'Жалобщик',
                        'en' => 'Şikayətçi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Sən olmasaydın',
                        'ru' => 'Если бы не было тебя',
                        'en' => 'Sən olmasaydın',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Sevgi hədiyyələri',
                        'ru' => 'Лекарство для смелости',
                        'en' => 'Sevgi hədiyyələri',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Cəsarət dərmanı',
                        'ru' => 'Сколько лет Санубар',
                        'en' => 'Cəsarət dərmanı',
                    ],
                    'files' => [
                    ],
                ],
            ],


            'right' => [
                [
                    'year' => '1959',
                    'name' => [
                        'az' => 'Təbrizin cavabı',
                        'ru' => 'Ответ Тебриза',
                        'en' => 'Təbrizin cavabı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1959',
                    'name' => [
                        'az' => 'Birinci dərs',
                        'ru' => 'Первый урок',
                        'en' => 'Birinci dərs',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1950',
                    'name' => [
                        'az' => 'Fəlakətli mərtəbələr',
                        'ru' => 'Трагические ступени',
                        'en' => 'Fəlakətli mərtəbələr',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1951',
                    'name' => [
                        'az' => 'Elsevər',
                        'ru' => 'Эльсевер',
                        'en' => 'Elsevər',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1951',
                    'name' => [
                        'az' => 'Dostluq',
                        'ru' => 'Дружба',
                        'en' => 'Dostluq',
                    ],
                    'files' => [
                    ],
                ],


                [
                    'year' => '1951',
                    'name' => [
                        'az' => 'Yaşarın səhvi',
                        'ru' => 'Ошибка Яшара',
                        'en' => 'Yaşarın səhvi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Sənubərin neçə yaşı var',
                        'ru' => 'Месть девушек',
                        'en' => 'Sənubərin neçə yaşı var',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Qızların intiqamı',
                        'ru' => 'Дитя',
                        'en' => 'Qızların intiqamı',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Övlad',
                        'ru' => 'Качели',
                        'en' => 'Övlad',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Yelləncək',
                        'ru' => 'Качели',
                        'en' => 'Yelləncək',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Kişilərə inanın',
                        'ru' => 'Верьте мужчинам',
                        'en' => 'Kişilərə inanın',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Molla hədiyyəsi',
                        'ru' => 'Подарок муллы',
                        'en' => 'Molla hədiyyəsi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Zəncirbənd',
                        'ru' => 'Молния',
                        'en' => 'Zəncirbənd',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Palitra və fırça',
                        'ru' => 'Палитра и кисть',
                        'en' => 'Palitra və fırça',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Eksperiment Ağasəlim',
                        'ru' => 'Экспериментатор Агаселим',
                        'en' => 'Eksperiment Ağasəlim',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1972',
                    'name' => [
                        'az' => 'Bir gecənin novellası',
                        'ru' => 'Новелла одной ночи',
                        'en' => 'Bir gecənin novellası',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1973',
                    'name' => [
                        'az' => 'Çempionatın final yarışında',
                        'ru' => 'На финальной игре чемпионата',
                        'en' => 'Çempionatın final yarışında',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1973',
                    'name' => [
                        'az' => 'Əjdər iş axtarır',
                        'ru' => 'Аждар ищет работу',
                        'en' => 'Əjdər iş axtarır',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1973',
                    'name' => [
                        'az' => 'Fərasətli oğul',
                        'ru' => 'Смекалистый сын',
                        'en' => 'Fərasətli oğul',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Cəbhədə hadisə',
                        'ru' => 'Случай на фронте',
                        'en' => 'Cəbhədə hadisə',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1974',
                    'name' => [
                        'az' => 'Hovuzda heykəl',
                        'ru' => 'Скульптура в бассейне',
                        'en' => 'Hovuzda heykəl',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1974',
                    'name' => [
                        'az' => '70-68 AZU',
                        'ru' => '70-68 АЗУ',
                        'en' => '70-68 AZU',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1974',
                    'name' => [
                        'az' => 'Bahar + məhəbbət = əla',
                        'ru' => 'Весна + любовь = отлично',
                        'en' => 'Bahar + məhəbbət = əla',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1974',
                    'name' => [
                        'az' => 'Yubiley gecəsi',
                        'ru' => 'Юбилейный вечер',
                        'en' => 'Yubiley gecəsi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1975',
                    'name' => [
                        'az' => 'Mango, 33, Şekspir',
                        'ru' => 'Манго, 33, Шекспир',
                        'en' => 'Mango, 33, Şekspir',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1975',
                    'name' => [
                        'az' => 'Hitlerin bacıoğlusu',
                        'ru' => 'Племянник Гитлера',
                        'en' => 'Hitlerin bacıoğlusu',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1975',
                    'name' => [
                        'az' => 'İki müavin, bir qadası',
                        'ru' => 'Один у  двух замов',
                        'en' => 'İki müavin, bir qadası',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '',
                    'name' => [
                        'az' => 'Altıbarmağ Güloğlanın sərgüzəştləri',
                        'ru' => 'Приключения шестипалого Гюльоглана',
                        'en' => 'Altıbarmağ Güloğlanın sərgüzəştləri',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1975',
                    'name' => [
                        'az' => 'Atabala Peyda',
                        'ru' => 'Атабала Пейда',
                        'en' => 'Atabala Peyda',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Ata',
                        'ru' => 'Отец',
                        'en' => 'Ata',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1965',
                    'name' => [
                        'az' => 'Toydan sonra nağara',
                        'ru' => 'Горчица после обеда',
                        'en' => 'Toydan sonra nağara',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1966',
                    'name' => [
                        'az' => 'Rezin adam',
                        'ru' => 'Резиновый человек',
                        'en' => 'Rezin adam',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1967',
                    'name' => [
                        'az' => 'Qapı',
                        'ru' => 'Дверь',
                        'en' => 'Qapı',
                    ],
                    'files' => [
                    ],
                ],


                [
                    'year' => '1967',
                    'name' => [
                        'az' => 'Xalam mənim dayımdır',
                        'ru' => 'Моя тетя мне дядя',
                        'en' => 'Xalam mənim dayımdır',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1980',
                    'name' => [
                        'az' => 'Əyri oturaq, düz danışaq',
                        'ru' => 'Поговорим начистоту',
                        'en' => 'Əyri oturaq, düz danışaq',
                    ],
                    'files' => [
                    ],
                ],


                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Metamorfoza',
                        'ru' => 'Метаморфоза',
                        'en' => 'Metamorfoza',
                    ],
                    'files' => [
                    ],
                ],


                [
                    'year' => '1980',
                    'name' => [
                        'az' => 'Gülməyəsən, neyləyəsən?',
                        'ru' => 'Смех – да и только!',
                        'en' => 'Gülməyəsən, neyləyəsən?',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Musa, oyuncaq və orijinallıq',
                        'ru' => 'Муса, игрушки и оригинальность',
                        'en' => 'Musa, oyuncaq və orijinallıq',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Qara qoçun düşüncələri',
                        'ru' => 'Размышления черного барана',
                        'en' => 'Qara qoçun düşüncələri',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Qorxu',
                        'ru' => 'Страх',
                        'en' => 'Qorxu',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Dublyonka',
                        'ru' => 'Дубленка',
                        'en' => 'Dublyonka',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '169',
                    'name' => [
                        'az' => 'Kefin necədir?',
                        'ru' => 'Как дела?',
                        'en' => 'Kefin necədir?',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Səfər yazıq neyləsin',
                        'ru' => 'Что делать бедному Сафару?',
                        'en' => 'Səfər yazıq neyləsin',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Skleroz',
                        'ru' => 'Склероз',
                        'en' => 'Skleroz',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1971',
                    'name' => [
                        'az' => 'Sevgi hədiyyələri',
                        'ru' => 'Любовные подарки',
                        'en' => 'Sevgi hədiyyələri',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Gülümsər qadın',
                        'ru' => 'Улыбающаяся женщина',
                        'en' => 'Gülümsər qadın',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'FatmaRestoranda',
                        'ru' => 'Фатима в ресторане',
                        'en' => 'FatmaRestoranda',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1970',
                    'name' => [
                        'az' => 'Kim necə cavab verərdi',
                        'ru' => 'Кто и как ответил бы',
                        'en' => 'Kim necə cavab verərdi',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1983',
                    'name' => [
                        'az' => 'Övlad',
                        'ru' => 'Дитя',
                        'en' => 'Övlad',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1983',
                    'name' => [
                        'az' => 'Yığdın, yadındadır',
                        'ru' => 'В памяти',
                        'en' => 'Yığdın, yadındadır',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1983',
                    'name' => [
                        'az' => 'Lirika, yumor, Züleyxa',
                        'ru' => 'Лирика, юмор, Зулейха',
                        'en' => 'Lirika, yumor, Züleyxa',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Dəzgahlar gül açanda',
                        'ru' => 'Когда расцветают станки',
                        'en' => 'Dəzgahlar gül açanda',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1969',
                    'name' => [
                        'az' => 'Sərgüzəşt',
                        'ru' => 'Приключение',
                        'en' => 'Sərgüzəşt',
                    ],
                    'files' => [
                    ],
                ],

                [
                    'year' => '1981',
                    'name' => [
                        'az' => 'Hamamda romans axşamı',
                        'ru' => 'Вечер романса в бане',
                        'en' => 'Hamamda romans axşamı',
                    ],
                    'files' => [
                    ],
                ],
            ],
        ],
    ],
];