$(document).ready(function() {

    $('#m_menu ul').lavaLamp({ speed: 700 });

    $('.downloaded_formats').hide();

    $('.creation li.download').hover(function() {
        $('.year', this).hide();
        $('.downloaded_formats', this).show();

    }, function() {
        $('.year', this).show();
        $('.downloaded_formats', this).hide();
    });


    $('.boxer').boxer({
        mobile: true
    }).on('keyup', function(e) {
        if (e.keyCode === 27) { //esc button
            $(".boxer").close();
        }
    });
});
