<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/{lang?}', 'PageController@showAbout');
Route::get('{lang?}/about', 'PageController@showAbout');
Route::get('{lang?}/creation', 'PageController@showCreation');
Route::get('{lang?}/multimedia', 'PageController@showMultimedia');
Route::get('{lang?}/press', 'PageController@showPress');
Route::get('{lang?}/press/show/{pressId?}', 'PressController@showPress');
Route::get('/download/{directory?}/{fileName?}', 'StorageController@downloadFile');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
