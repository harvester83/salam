<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PressController extends Controller
{
    /**
     * @param int $pressId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPress($lang, $pressId) {
        $content = view('press.' . $pressId);
        return view('press.press', ['content' => $content]);
    }
}
