<?php

namespace App\Http\Controllers;

use Config;

class PageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAbout()
    {
        return view('pages.about_' . app()->getLocale());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCreation()
    {
        $creations = Config::get('creation');
        return view('pages.creation', array('creations' => $creations));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMultimedia()
    {
        $multimedia = Config::get('multimedia');
        return view('pages.multimedia', array('multimedia' => $multimedia));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPress()
    {
        return view('pages.press_' . app()->getLocale());
    }
}
