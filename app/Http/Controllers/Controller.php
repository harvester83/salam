<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $currentLang = self::$router->getCurrentRoute()->getParameter('lang');
        if (!in_array($currentLang, array('az', 'ru', 'en'))) {
            $currentLang = 'az';
        }

        app()->setLocale($currentLang);
    }
}
