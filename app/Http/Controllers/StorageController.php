<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Response;
use Storage;

class StorageController extends Controller
{
    /**
     * @param string $directory
     * @param string $filename
     * @return $this
     */
    public function downloadFile($directory, $filename) {
        $file = Storage::disk('local')->get('public/' . $directory . '/' . $filename);

        return (new Response($file, 200))
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-Disposition', 'attachment; filename='.basename($filename));
    }
}
