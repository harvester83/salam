<?php

return [
    'title' => 'SALAM QƏDİRZADƏ. Məşhur Azərbaycan yazıçısı, dramaturq.',
    'description' => 'Məşhur Azərbaycan yazıçısı, dramaturq.',
    'keywords' => 'Salam Qədirzadə, Azərbaycan, Bakı, məşhur, yazıçı, dramaturq, roman, hekayə, povest, komediya, teatr, tamaşa, ədəbiyyat, incəsənət, yumor, satira, tənqid, gülüş, söz ustası, gülüş ustası, yumor ustası, satira ustası, novella, sovet ədəbiyyatı, personaj, qəhrəman, obraz',
    'site_created' => 'Sayt 2013-cü ildə yaradılmışdır.  Bütün hüquqlar qorunur.',

    'about' => 'HƏYATI',
    'creation' => 'YARADICILIĞI',
    'multimedia' => 'MULTİMEDİA',
    'press' => 'PRESSA',

    'contact' => 'Bizə yazın',

    'fb_description' => 'Salam Dadaş oğlu Qədirzadə 1923-cü ilin aprel ayının 10-da Bakı
                    şəhərində anadan olub. Hələ uşaq illərindən ədəbiyyata böyük həvəsi olan S.
                    Qədirzadə şeir və hekayələr yazır, ədəbi dərnəklərdə iştirak edir, məktəbdən
                    sonra məhz bu sahədə təhsil almağı planlaşdırırdı. Lakin Böyük Vətən müharibəsi
                    həmin arzuları düz 5 il təxirə saldı.',
];
