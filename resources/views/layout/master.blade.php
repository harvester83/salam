<!DOCTYPE html>
<html lang="<?php echo app()->getLocale(); ?>">

<head>
    <title><?php echo trans('common.title'); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="<?php echo trans('common.description'); ?>">
    <meta name="keywords" content="<?php echo trans('common.keywords'); ?>">
    <meta name="reply-to" content="">
    <meta name="robots" content="index, follow">
    <meta name="rating" content="General">
    <meta name="copyright" content="">

    {{--Twitter Card data--}}
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="http://salamqadirzade.az/">
    <meta name="twitter:title" content="<?php echo trans('common.title'); ?>">
    <meta name="twitter:description" content="<?php echo trans('common.fb_description'); ?>">
    <meta name="twitter:creator" content="" >
    <meta name="twitter:image" content="http://salamqadirzade.az/img/photo.png">

    {{--facebook--}}
    <meta property="fb:app_id" content="1182019341829893" />
    <meta property="og:title" content="<?php echo trans('common.title'); ?>">
    <meta property="og:type" content="article">
    <meta property="og:url" content="http://salamqadirzade.az/">
    <meta property="og:image" content="http://salamqadirzade.az/img/portrait.png">
    <meta property="og:description" content="<?php echo trans('common.fb_description'); ?>">

    <link rel="shortcut icon" type="image/x-icon"  href="{{ URL::asset('img/favicon32x32.ico') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('img/favicons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('img/favicons/favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{ URL::asset('css/html5reset.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.fs.boxer.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/lightbox-lb.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
</head>

<body>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-76283410-1', 'auto');
    ga('send', 'pageview');
</script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1182019341829893',
            xfbml      : true,
            version    : 'v2.6'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="wrapper">

    <header>

        <div class="top-header">
            @include('partials.lang-switcher')
            <i class="sprite sprite-contour_top"></i>
        </div>


        <div class="top-main">
            <div class="title">
                <h1 class="name">
                    <a href="<?php echo url('/' . app()->getLocale()); ?>"> <?php echo trans('common.title'); ?> </a>
                </h1>

                <h2 class="activities"><?php echo trans('common.description'); ?></h2>

                <div class="aphorisms">
                    <?php
                        $aphorisms = [
                            'az' => [
                                'Qəhrəman olmaq üçün bəzən bir an kifayətdir, ləyaqətli insan olmaq üçün ömür boyu çalışmaq lazımdır.',
                                'Biz dünyadan gedəndən sonra nə qədər xatırlanırıqsa, demək yenidən bir o qədər yaşayırıq.',
                                'Başqalarının kədərini, sevincini onlardan da artıq yaşamayan yazıçı sənətkar deyil.',
                                'Ağac kökü ilə möhkəmlənir, insan dostu ilə.',
                                'Ən böyük səfərlər ilk addımdan başlayır.',
                                'Əbədi  yaşamaq üçün birinci növbədə ölmək lazımdır.',
                                'Hər gün ömürdən gedir.',
                            ],
                            'ru' => [
                                'Чтобы стать героем,  порой достаточно одного мига. Чтобы стать достойным человеком, нужно трудиться всю жизнь.',
                                'Пока нас вспоминают после смерти, мы продолжаем жить.',
                                'Писатель, не переживающий боль и радость других больше них самим, не может быть настоящим мастером.',
                                'Дерево крепчает корнями, человек – друзьями.',
                                'Самые великие походы начинаются с первого шага.',
                                'Чтобы жить вечно, сначала надо умереть.',
                                'Каждый день уходит из жизни.',
                            ],
                            'en' => [
                                'Qəhrəman olmaq üçün bəzən bir an kifayətdir, ləyaqətli insan olmaq üçün ömür boyu çalışmaq lazımdır.',
                                'Biz dünyadan gedəndən sonra nə qədər xatırlanırıqsa, demək yenidən bir o qədər yaşayırıq.',
                                'Başqalarının kədərini, sevincini onlardan da artıq yaşamayan yazıçı sənətkar deyil.',
                                'Ağac kökü ilə möhkəmlənir, insan dostu ilə.',
                                'Ən böyük səfərlər ilk addımdan başlayır.',
                                'Əbədi  yaşamaq üçün birinci növbədə ölmək lazımdır.',
                                'Hər gün ömürdən gedir.',
                            ],
                        ];
                        $aphorisms = $aphorisms[app()->getLocale()];
                        $aphorismKey = array_rand($aphorisms);
                    ?>
                    <p class="active">
                        <i class="sprite sprite-quote_open"></i>
                        <?php echo $aphorisms[$aphorismKey] ?>
                        <i class="sprite sprite-quote_close"></i>
                    </p>
                </div>
            </div>

            <div class="portrait">
                <img src="{{ URL::asset('img/portrait.png') }}" alt="<?php echo trans('common.title'); ?>">
            </div>

            <div class="clearer"></div>
        </div>

        @include('partials.navigation')

        <div class="clearer"></div>
    </header>

    @yield('content')

    <footer>
        <i class="sprite sprite-contour_bottom"></i>
        <p><?php echo trans('common.site_created'); ?></p>

        @include('partials.social-bar')

        <div class="clearer"></div>
    </footer>
</div>

<script type="text/javascript" src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/lightbox-2.6.min-lb.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.lavalamp.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.fs.boxer.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
</body>
</html>