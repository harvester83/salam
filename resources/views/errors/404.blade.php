@extends('./layout.master')

@section('content')
    <div class="content error">
        <p><span>404</span>səhifə tapilmadı. <a href="<?php echo url('/'); ?>">Ana səhifəyə qayıdın</a></p>
    </div>
@endsection