@extends('layout.master')

@section('content')
    <div class="content">
        <div class="collons">
            <?php echo $content ?>
            <div class="clearer"></div>
        </div>
    </div>
@endsection