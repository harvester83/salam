<article class="press_post">
    <h2>"Məhəbbət Vətənə qalib gəldi!"</h2>

    <div class="author">
        <p>
            Ötən əsrin 50-ci illərində ədəbiyyata gələn yazıçı Salam Qədirzadə o dövr oxucularının böyük maraq
            göstərdiyi və
            sevdiyi nasirlərdən idi. Cəmi 64 il yaşamış görkəmli ədibin bu bahar 90 yaşı tamam olur. O, 1923-cü ilin
            aprelində
            Bakıda dünyaya gəlib. Uşaqlıq və gənclik dostu, biz nəsil jurnalistlərin müəllimi Nurəddin Babayev
            xatirələrində
            bildirirdi ki, onlar yeniyetmə çağlarında dörd dost olublar. Şıxəli Qurbanov, Bəşir Səfəroğlu, Salam
            Qədirzadə
            və
            Nurəddin müəllimin özü. Onların ağıllarına da gəlməzdi ki, illər keçəcək, bu dostların hər birinin
            Azərbaycan
            mədəniyyətində, ədəbiyyatında, ictimai-siyasi həyatında öz yerləri olacaq. Şıxəli Qurbanov dövlət xadimi,
            yazıçı
            kimi seviləcək. Bəşir Səfəroğlu Azərbaycan səhnəsinin parlaq ulduzuna çevriləcək. Hətta dostu Salam
            Qədirzadənin
            tiplərini yaradacaq. Yazıçının povest və romanları minlərlə oxucunun qəlbinə yol tapacaq, seviləcək.
            Nurəddin
            Babayev uzun illər jurnalistika fakültəsində dərs deyəcək, əsərləri ilə bədii publisistikaya yeni nəfəs
            gətirəcək.
        </p>

        <p>Salam Qədirzadənin gəncliyi İkinci Dünya müharibəsinin odlu-alovlu səngərlərində keçib. Müxtəlif cəbhələrdə
            faşistlərə qarşı ölüm-dirim mübarizəsində igidlik göstərən bu vətən oğlu döyüş hünərinə görə üçüncü dərəcəli
            "Şöhrət" ordeni və 12 medalla təltif olunub. Müharibədən sonra Bakıya qayıdan Salam Qədirzadə ADU-nun
            filologiya fakültəsinə daxil olsa da, üçüncü kursdan Moskvaya, M.Qorki adına Ədəbiyyat İnstitutuna
            dəyişilib. O, burada şeir bölməsində təhsil alıb. Bu ali məktəbdə Nəbi Xəzri, Əli Kərim, İsgəndər Coşqun,
            Cabir Novruz, İsa Hüseynov, Salam Qədirzadə bir yerdə oxuyublar. Belə bir fakt da var ki, yataqxanada yer
            olmadığından azərbaycanlı tələbələr yazıçı A.Fadeyevin Moskva kənarındakı bağ evində qalıblar. Xalq şairi
            Səməd Vurğun da Moskvaya yolu düşəndə tələbələrə baş çəkər, onların dərd-səri ilə maraqlanar, bəzən də
            restorana aparıb qonaqlıq verər, nəyə ehtiyacı olduqlarını öyrənər, ciblərinə pul qoyar, hətta qışda birinə
            şərfini, birinə paltosunu, birinə də papağını... bağışlayıb. Salam Qədirzadə şairin bu xeyirxahlığını həmişə
            minnətdarlıqla xatırlayardı. Tale elə gətirdi ki, həyat yoldaşını da elə bu institutda təhsil alan qızların
            arasından tapdı Salam Qədirzadə.</p>

        <p>Onun ilk əsəri 1948-ci ildə "Pioner" jurnalında işıq üzü görən "Oqtay" adlı hekayəsi olub. Həmin vaxtdan
            ömrünün sonuna kimi dövri mətbuatda müntəzəm çıxış edən Salam Qədirzadənin ictimaiyyət arasında ədəbi
            nailiyyəti, oxucular tərəfindən çox sevilən, haqqında tənqidçilərin xoş sözlər dediyi "Qış gecəsi" romanı
            isə böyük müvəffəqiyyət qazandı. Bu roman bir yaz nəfəsi kimi gəncliyin mənəvi dünyasına yol tapdı. Burada
            gənclərin sosial həyatları ilə bağlı mətləblərə toxunulurdu. Müharibədən sonrakı gənclərin arzularından,
            xüsusilə də kolxozda çalışanların həyatından bəhs edilirdi.</p>

        <p>O dövrdə gənclərin ən çox oxuduğu, müzakirə etdiyi, mübahisəyə, bəzən də tənqidə məruz qalan yazıçılardan
            biri idi Salam Qədirzadə. Müəllifin "Kəndimizdə bir gözəl var", "Sevdasız aylar", "Ulduzlar sayrışanda"
            kitabları əl-əl gəzərdi. Bu əsərlərin adları, təsvir olunan hadisələr, mövzunun əlvanlığı, gənclərin duyğu
            və düşüncələri o qədər səmimi, şirin və təsirli dillə qələmə alınmışdı ki, kitabı əlinə alan kəs ondan
            ayrıla bilmirdi. Salam Qədirzadə oxunan və sevilən yazıçı idi. Yazıçı üçün də oxucu rəğbəti və məhəbbəti
            qazanmaq ən böyük mükafatdır. Çünki yazıçının canı elə oxucudadır. Onu yaşadan da oxucudur. Salam
            Qədirzadənin əsərlərinin həyatiliyinə, inandırıcılığına, təbiiliyinə, qəhrəmanlarının reallığına heyran
            olmamaq mümkün deyildi. "Ülkər Aytəkin", "46 bənövşə" povestlərini oxuduqca adama elə gəlirdi ki, hər gün
            rastlaşdığı, ünsiyyət bağladığı yüksək duyğulu, dəyanətli, mətanətli insanlarla görüşür. O dövrdə Azərbaycan
            ədəbiyyatında güclü ənənəyə malik olan hekayə janrının bir növ zəifləməsi, seyrəlməsi hiss olunurdu. Hətta
            vaxtilə gözəl hekayə müəllifləri olan nasirlər də sanki bu janrda yazmağı unutmuşdular. Bax, belə bir vaxtda
            Salam Qədirzadənin hekayələrdən ibarət kitabları bu solmuş gülüstana yeni həyat gətirdi. "Mənim
            ceyranlarım", "Həyat lövhələri", "Hekayələr", "Bir qız bir oğlanındır", "Təbəssümün mükafatımdır" və başqa
            əsərləri insanları həm güldürdü, həm də düşündürdü. Salam Qədirzadə yumoristik üslubda hekayələr, dramlar
            yazan müəllif idi. Elə bu baxımdan da Azərbaycan teatrları onun yaradıcılığına tez-tez müraciət edir,
            dramlarına səhnə həyatı verirdilər. Xüsusilə də komediyaları rejissorların diqqətini çəkirdi. Bakının,
            Gəncənin, Naxçıvanın, Dağıstanın, Şəkinin teatr səhnələrində tamaşaya qoyulmuş "Şirinbala bal yığır",
            "Hardasan, ay subaylıq", "Həmişəxanım", "Gurultulu məhəbbət", "Gözəllik ondur" kimi komediyaları tamaşaçılar
            tərəfindən rəğbətlə qarşılanıb. Salam Qədirzadə cəmiyyətdəki qüsurları gülə-gülə islah etməyi bacaran yazıçı
            idi.</p>

        <p>Onun mətbuatımızın inkişafında da böyük xidmətləri olub. 1952-1953-cü illərdə "İnqilab və mədəniyyət" jurnalı
            redaksiyasında ədəbi işçi, məsul katib vəzifələrini daşıyıb. Salam Qədirzadə uzun müddət "Kirpi" satirik
            jurnalı redaksiyasında məsul katib, baş redaktor vəzifələrində çalışıb. Yazıçı və jurnalist kimi sovet
            nümayəndə heyətinin tərkibində İtaliya, Fransa, Yunanıstan, Rumıniya, Finlandiya, Belçika, Türkiyə
            ölkələrində səfərdə olub. O zaman Azərbaycan yazıçısının çox sayda xarici ölkədə olması hər kəsə qismət
            deyildi. Səfərdən sonra Salam Qədirzadənin qələmə aldığı təəssüratlar müxtəlif mətbuat orqanlarında dərc
            edilərdi. Yazıçının müşahidələri ya hekayə, ya da məqalə və oçerk vasitəsilə oxuculara çatdırılardı. Salam
            Qədirzadənin əsərləri, eyni zamanda keçmiş SSRİ və xarici ölkə xalqlarının dillərinə tərcümə edilmişdir.</p>

        <p>Sağlığında 18 kitabı işıq üzü görən Salam Qədirzadənin son əsəri "Hər gün ömürdən gedir" adlanır. Keçirdiyi
            ağrı-acılar sanki yazıçıya həyatdan tez gedəcəyini agah etmişdi. Bütün yarımçıq işlərinə əl gəzdirərək
            yazılarını tamamlamağa çalışarmış. Təəssüf ki, "Hər gün ömürdən gedir" kitabının çapını görmək ona qismət
            olmayıb. Həmişə deyib-gülən, şən ovqatlı, oxucularına da gülüş dolu əsərlər bəxş edən Salam Qədirzadə üç
            dəfə insult keçirib. Dünyaya "əlvida" dediyi gündən 26 il ötür. Adını çəkdiyimiz və çəkmədiyimiz əsərləri
            isə yazıçını yaşatmaqdadır. Salam Qədirzadə yaradıcılığını sevən bir oxucu kimi düşünürəm ki, biz belə
            unudulmaz yazıçıların əsərlərinin gələcək nəslə təbliği sahəsində üzərimizə düşən vəzifələri ləyaqətlə
            yerinə yetirməliyik. Bu missiyanın birinci yolu kimi yazıçının seçilmiş əsərlərinin latın qrafikası ilə çapı
            çox vacibdir. Çünki oxucular "Sarmaşıqlı aynabənd", "Şərəfnisə qışqırır", "Başabəla Paşabala", "Burada insan
            yaşamışdır", "Qar üstə qızılgül", "Sən olmasaydın", "Sevginin ünvanı ürəkdir" və s. əsərləri mütləq
            oxumalıdırlar.</p>

        <p>Azərbaycan nəsrində öz imzasını bitkin əsərləri ilə əbədiləşdirən Salam Qədirzadə repressiyanın, müharibənin,
            quruluşun dəhşətini görmüş, acılarını yaşamış, dəfələrlə haqsızlıqla üzləşmiş qələm sahiblərindən biri idi.
            Belə bir fikir var ki, güldürməyi, gülə-gülə düşündürməyi bacaran sənətkarlar həmişə ən çox qəm çəkən,
            içində dərd gəzdirən və az yaşayan olurlar. Amma onların əsərləri həmişə gələcəyə doğru inamla addımlayır.
            Salam Qədirzadə də bu qismətdən uzaq düşmədi. Povest, roman, hekayə və dramları ilə Azərbaycan ədəbiyyatında
            həmişəlik yaz nəfəsidir!</p>

        <div class="comment">
            <div class="post-date">
                <div class="dots">...</div>
                <p>F.Xəlizadə,"Azərbaycan" qəzeti</p>
                <div><span class="date">19 aprel 2013</span></div>
            </div>
        </div>
    </div>
</article>
