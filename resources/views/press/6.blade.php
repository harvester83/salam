<article class="press_post">
    <h2>Ürəklərdə yeri olan sənətkar.</h2>

    <div class="author paragraphs_block">
        <mark>Kirpi jurnalı.</mark>
        <p>
            Azərbaycan ədəbiyyatının görkəmli nümayəndələrindən biri olan Salam Qədirzadə ilə mənim ilk tanışlığım 1977-ci ildə olub. Əvvəllər məsul katib işlədiyi  «Kirpi» jurnalına təzəcə baş redaktor təyin olunmuşdur. O vaxt mən xalq şairi, son dərəcə gözəl insan olam Məmməd Arazın redaktoru olduğu «Azərbaycan təbiəti» jurnalında çalışırdım. </p>

        <p>
            Bir gün Salam müəllimlə rastlaşdım. «Bakı» qəzetundə dərc olunan yazılarımdan məni tanıdığını dedi.  «Kirpi» jurnalına işə dəvət etdi. Müəyyən tərəddüdlərdən sonra razılıq verdim. Məni jurnalın məsul katibi vəzifəsinə təyin etdi. O vaxt gənc jurnalist üçün Mərkəzi Komitənin nomenklaturunda olan vəzifədə işləmək, şan-şöhrəti hər yerə yayılmış bir jurnalda çalışmaq həm şərəfli, həm də məsuliyyətli idi. Buna görə də indi anadan olmasının 85 illik yubileyini qeyd etdiyimiz görkəmli yazıçı Salam Qədirzadə haqqında xatirələrimi və ürək sözlərimi bildirməyi özümə şərəf sayıram.
        </p>
    </div>

    <div class="author">
        <p>
            Salam müəllimi oxuculara təqdim etməyə, əslində, heç bir ehtiyac yoxdur. Çünki gəncliyimiz onun bir-birindən
            gözəl, duzlu-məzəli əsərlərindən keçib. Təəssüflər olsun ki, bu gün görkəmli yazıçı haqqında keçmiş zamanda
            danışmaq məcburiyyətindəyəm. Çünki o, artıq xeyli vaxtdır ki, aramızda yoxdur. ..
        </p>

        <p>İlk addımı uğurlu olub. Qorki adına Ədəbiyyat İnstitutunda oxuyarkən yazdığı ilk povesti Moskvada «Detgiz»
            nəşriyyatında çap olunub və diplom işi kimi qəbul edilib. Sonra onun qələmindən çıxan «Qış gecəsi», «Burada
            insan yaşamışdır», «Su pərisi Marianna», «Başabəla Paşabala», «Ömrün təzə illəri», «Kəndimizdə bir gözəl
            var», «46 bənövşə» kimi roman və povestləri oxucuların rəğbətini qazanmışdır. Bu əsərlər başqa xalqların
            dilinə də tərcümə edilmişdir. </p>

        <p>O, satirik əsərlərində həyatdakı neqativ halları, bürokrt və rüşvətxorları, xalq malını talayanları tənqid
            atəşinə tutur, öz yumoru ilə oxucuları güldürə-güldürə düşündürürdü. Böyük əsərlərində isə qayğıkeş
            insanlara xoş duyğular aşılayır, saf məhəbbəti, gəncliyi tərənnüm edirdi. S. Qədirzadə həm də «Şirinbala bal
            yığır», «Hardasan, ay subaylıq ?», «Qış gecəsi», «Həmişəxanım», «Gurultulu məhəbbət», «Gözəllik ondur» kimi
            respublikamızda və onun hüdudlarından kənarda tamaşaya qoyulan komediyaların müəllifidir. </p>

        <p>Salam müəllim dostluqda möhkəm idi. Onun dostlarından bəzilərini tanıyırdım. Dillərindən Salam haqqında çoxlu xoş sözlər eşitmişdim. Onlardan biri – bir neçə il əvvəl dünyasını dəyişmiş müəllim Əli Rüstəmov Tohidinin - Salam Qədirzadə haqqında dediyi sözlər indi də xatirimdədir:</p>
    </div>

    <div class="comment paragraphs_block">
        <p>
            «Mənim dostlarım çox olub. Amma Salam dost aləmində nadir idi, əvəzsiz idi. Dilinə yalan gətirməzdi. Sözünün yiyəsi idi. Heç vaxt vədinə xilaf çıxmazdı. Çox diqqətli, qayğıkeş idi».
        </p>
    </div>

    <div class="author paragraphs_block">
        <p>
            Salam müəllim dostları ilə birlikdə yeyib-içməyi, qonaqlıqda olmağı çox sevirdi. Bir gün işdən sonra mənə dedi ki, evə getməyə tələsmə, səninlə bir yerə gedəcəyik. Yenicə ailə həyatı qurmuşdum. İşdən sonra çox vaxt evə yollanırdım. Evə gedə bilməyəndə isə telefonla xəbər verirdim ki, narahat olmasınlar. Həmin gün evə zəng etməyi unutmuşdum. Nə isə, redaksiyamızın xidməti maşınına oturduq. Salam müəllim sürücüyə dedi ki, Sumqayıta, bacımgilə gedəcəyik. Öz-özümə fikirləşdim ki, bacısı, bacısı uşaqları üçün darıxıb, uzağı bir-iki saata qayıdarıq. Odur ki, heç evə də zəng eləmədim (o vaxt cib telefonu yox idi),
        </p>
    </div>

    <div class="author paragraphs_block">
        <p>
            Bacısıgil həyət evində yaşayırdılar. Onu görəndə çox sevindilər. Bir göz qırpımında əziz qonağın şərəfinə qoç kəsildi, manqal yandırıldı. Çay süfrəsi yığışdırılan kimi ortaya duz-çörək, kabab gəldi. Təbii ki, həmin gün qonaqlıq mənim təsəvvür etdiyim kimi bir-iki saat deyil, gecəyarıyadək çəkdi. Sonra  maşına minib, Bakıya qayıtdıq. Sürücü Salam müəllimi düşürüb, məni evə apardı. Artıq səhər açılırdı. Həmin gün həyatımda ilk dəfə idi ki, evə xəbər verməmiş belə gec gəlirdim. Evdə biləndə ki, Salam müəllimlə birlikdə olmuşam, heç nə demədilər...
        </p>

        <p>Şəxsi arxivimdə bir xatirə kimi saxladığım şəklə baxdıqca xəyalən 32 il əvvələ qayıdır, o unudulmaz yazıçı
            ilə bağlıb xatirələri təzədən yaşayıram.</p>

    </div>

    <div class="comment">
        <div class="post-date">
            <div class="dots">...</div>
            <p>Həmid Nəcəfquliyev</p>

            <div><span class="date">yanvar 2010</span></div>
        </div>
    </div>
</article>