<article class="press_post">
    <h2>"Məhəbbət Vətənə qalib gəldi!"</h2>

    <mark>Leyla Qədirzadə atası Salam Qədirzadə haqqinda:</mark>
    <div class="comment">
        <p>Kəndimizdə bir gözəl var - adı Leyladır onun "... Hər dəfə atamın - yazıçı Salam Qədirzadənin qələminə məxsus
            bu sətirlər yadıma düşəndə qəlbimi fərəh bürüyür. Düşünürəm: "O Leyla mənəm". Əlbəttə ki, mən atamın
            "Kəndimizdə bir gözəl var" əsərində həyatından bəhs etdiyi qız deyiləm. Sadəcə, romanın müəllifi həmin əsəri
            mənə - yeganə qızına ithaf edib. Eləcə də "Su pərisi Mariana" əsəri anam Roza xanıma, "Sevdasız aylar" isə
            qardaşım Ruslana həsr olunub.</p>

        <p>Atama qədər nəslimizdə heç kim yazıçılığa meyl göstərməyib. Onun valideynləri sadə adamlar olublar. Ticarətlə
            məşğul olan atası özünün, ailəsinin ruzusunu balaca dükanından gələn gəlirdən çıxarıb. Anası Ziba isə elə
            evlərində dərzilik edib. Nənəmin də, babamın da əsilləri Maştağadandır. Sonradan Bakıya köçüblər. Atam bu
            şəhərdə 1923-cü il aprelin 10-da dünyaya göz açıb. Evin ikinci uşağı olub. Ondan böyük bir bacısı varmış.
            Atamdan sonra ailələrində dörd qız, bir oğlan uşağı da dünyaya gəlib.</p>

        <p>O, orta təhsil aldığı 150 saylı məktəbdə keçən illərini həmişə minnətdarlıqla xatırlayardı. Qardaşımla mən
            balaca olanda bizi gəzməyə çıxarar, çox vaxt da yolunu mütləq həmin məktəbin yanından salardı. Ora çatanda
            ayaq saxlayar, məktəbin binasını bizə göstərər, qürurla "Mən burada oxumuşam", - deyərdi.</p>

        <p>Lap uşaq yaşlarından atamın ailələrində xüsusi yeri olub. Qardaş-bacıları onu həddindən artıq çox istəyib,
            böyük qardaşlarıyla fəxr ediblər. Evdə ona - ailənin ən istedadlı uşağına oxuması, yaradıcılıqla məşğul
            olması üçün şərait yaradılıb. Atamın yazıçılıq həvəsi orta məktəbdə yazdığı şeirləri ilə üzə çıxıb. Ancaq o
            dövrdə onu təkcə ədəbiyyat maraqlandırmayıb. Musiqini o qədər sevib ki, hətta tar çalmağı öyrənməyə qərar
            verib. Bu sahədə təhsil almaq üçün sənədlərini götürüb musiqi məktəbinə gedib. Onların musiqi qabiliyyətini
            bəstəkar Üzeyir Hacıbəyov yoxlayırmış. Növbə atama çatıb. O, tarda bir musiqi ifa edib. Üzeyir bəy deyib ki,
            bala, sən bu sənəti at, tarzən olmaq sənlik deyil.</p>

        <p>Atam bu xatirəni arabir yada salar, gülə-gülə bizə danışar, sonra ona doğru yol göstərən Üzeyir bəyə rəhmət
            oxuyardı. Çünki bəstəkarın məsləhətindən sonra o, ciddi düşünüb: axı rəssam ola bilərdi, istedadı var idi,
            yaxşı şəkillər, şarjlar, karikaturalar çəkirdi. Bəs bunların hamısından qabaq, lap kiçik yaşlarından onu
            cəlb edən yazıçılıq istəyi? Hansı sənətin sahibi olur-olsun, bir gün hər şeyi atıb ona doğru can atacağını
            hiss etmirdimi? Artıq yazmağa başladığı şeirləri də pis alınmırdı. Bundan sonra bir müddət tərəddüd edib:
            rəssam olsun, yoxsa yazıçı-şair? İllər keçdikcə ədəbiyyata olan marağı rəssamlıq həvəsini üstələyib.</p>

        <p>Yeddi uşağının heç birinin, necə deyərlər, hələ əlləri çörəyə çatmamış babam vəfat edib. Ondan sonra çətin
            vəziyyətdə qalıblar. Nənəm heç yerdə işləmirdi, evdə sifariş götürür, dərzilik edirdi. Ancaq onun da qazancı
            çox deyildi. Ailənin əsas qayğılarını böyük oğul öhdəsinə götürüb, atam bacılarına, qardaşına həm ata olub,
            həm də qardaş. O həm təhsilini davam etdirmək, həm də həyatlarını düzəninə qoymaq üçün yollar axtarıb,
            planlar qurduğu vaxt Böyük Vətən müharibəsi başlayıb. Orta məktəbi yenicə başa vuran, on səkkiz yaşlı gənc
            hər şeyi pərən-pərən qoyub könüllü olaraq cəbhəyə yollanıb. Müharibədə rabitəçi olub. Döyüşlər zamanı
            dəfələrlə yaralanıb, dönə-dönə ölümlə üzləşib. Bakıya 1945-ci ildə - Böyük Vətən müharibəsi başa çatandan
            sonra qayıdıb. Beşillik ayrılıqdan dönən atam həyatını yenidən qurmaq, oxuyub təhsil almaq və söz yox ki,
            yazıçılıqla məşğul olmaq istəsə də, əvvəlcə işləməli, bacılarına, qardaşına, anasına maddi cəhətdən kömək
            etməli olub. Bir müddət Sumqayıtın salınmasında iştirak edib. İlk əsəri olan "Gənclik" povesti də elə o
            illərə həsr olunub.</p>

        <p>Maddi cəhətdən vəziyyəti ürəkaçan olmasa da, oxumaq sevdası onu Moskvaya, Maksim Qorki adına Ədəbiyyat
            İnstitutuna aparıb. Məktəbə qəbul olunub. Arzulu, macəralı tələbəlik həyatı başlayıb. Atam Moskvada Nəbi
            Xəzri, Əli Kərim, İsa Hüseynovla bir dövrdə oxuyub, yataqxanada eyni otaqda qalıb, möhkəm dost olublar.
            Qayğıları da bir olub. Kasıb tələbə gənclər təqaüdlə güclə dolanıb, çox vaxt heç çörək almağa belə pul
            tapmayıblar. Atam danışardı ki, elə ac vaxtlarımız olurdu ki… Moskva yeməkxanalarında o zamanlar yemək
            masalarının üstünə həmişə çörək və xardal qoyurdular, bunlar pulsuz idi. Atamgil də yeməkxanaya gedib o
            çörəklə xardalı yeyirmişlər ki, ürəklərində durum olsun.</p>

        <p>O, özlərindən əvvəlki ədəbi nəslin nümayəndələrindən yalnız Səməd Vurğunun adını tez-tez çəkər, onu sadə,
            xeyirxah adam kimi yada salardı. Tələbəlik çağlarında şairin onlara necə kömək etdiyini xatırlayardı.
            Moskvaya tez-tez gedən Səməd Vurğun azərbaycanlı tələbələrə baş çəkər, hər dəfə də restorana aparıb qonaqlıq
            verərmiş. Bayramlarda Moskvada Ədəbiyyat İnstitutunda oxuyan gənclərin paxlava, şəkərbura paylarını Səməd
            Vurğun apararmış. Onların ehtiyac içində yaşadıqlarını, korluq çəkdiklərini görən şair bəzən soyuq qış
            aylarında birinə şərfini, birinə papağını verib.</p>

        <p>Atamgil institutda oxuyanda məşhur bir evdə - yazıçı Fadeyevin bağ evində qalıblar. Oranı sonralar
            yaradıcılıq evinə çevirdilər. Amma atamgilin təhsil aldıqları illərdə Fadeyevin bağ evi yataqxana binası
            kimi tələbələrin ixtiyarında olub. Azərbaycanlı tələbələr - Əli Kərim, Nəbi Xəzri, İsgəndər Coşğun, Cabir
            Novruz, İsa Hüseynov və atam hər gün Peredelkinodan şəhərə elektrik qatarıyla gedib-gəliblər. Hər səhər
            qatarda çox gözəl, sarışın, gənc bir qızla yol-yoldaşı olublar. Ətrafında baş verənləri o, elə bil, heç
            görmür, diqqətlə dizlərinin üstünə qoyduğu kitabı oxuyurmuş. Qızın hər gün həvəslə, yorulmadan mütaliə
            etməsi tələbələri maraqlandırıb. Yaxınlaşıb tanış olublar. Roza adlı moskvalı qız gənclərin Azərbaycandan
            gəldiyini, Ədəbiyyat İnstitutunda təhsil aldıqlarını biləndə sevinib. Çünki o da ədəbiyyatı həddindən artıq
            sevər, mütəmadi olaraq bədii əsərlər oxuyarmış. Söhbətləri tutub. O gündən Peredelkinodan Moskvaya gedən
            elektrik qatarı gənclərin ədəbiyyat barədə müzakirə, fikir mübadilələri məkanına çevrilib. Getdikcə
            bir-birlərini daha yaxşı tanıyıblar. Bir gün azərbaycanlı tələbələrdən biri - Salam Qədirzadə anlayıb ki,
            qıza vurulub. Bu sevdadan dostları da xəbər tutublar. Atam hələ cürət edib ürəyini açmırmış. Amma yəqin
            bilsəydi ki qızın da bu qədər tələbənin arasında gözü onu tutub, tərəddüd etməzdi. Atam bir gün cəsarətini
            toplayıb qıza könül açıb, məhəbbətlərinin qarşılıqlı olduğunu öyrənəndə, sanki dünyanı ona bağışlayıblar.
            İlk görüşlərində bir-birlərinə ayrı keçən illərindən danışıblar. Anam ondan heç nəyi gizlətməyib. Amma atam
            bir məsələni sevgilisindən sirr saxlayıb, ona Bakıda heykəltəraş qızla nişanlı olduğunu deməyib.</p>

        <p>1952-ci ildə atamın tələbəlik illəri sona yetir. Moskvayla vidalaşmaq vaxtı yaxınlaşır. Ancaq könlünü ovlayan
            gözəldən həmişəlik ayrılmağı ağlına da gətirmir. Elə qız da onsuz yaşamaq barədə düşünmək belə istəmir.
            Dostlarını restoranların birinə toplayır, xudmani məclis qurub evlənirlər. Atamın Moskvadan gözəl bir qızla
            dönməsi anasını, qardaş-bacılarını çaşdırsa da, giley-güzar etmirlər. Həm ona görə ki, atamı çox istəyir,
            onu özlərinə ağsaqqal bilir, bir sözlərini iki etmirdilər, həm də anlayırdılar - könülə güc yoxdur.
            Sevgilisinə görə doğmalarını, yurdunu qoyub-gələn qərib gəlini incitməyə də ürəkləri gəlmir. Ona "Salamın
            nişanlısı var", - deməyə utanırlar. Gəlini mehribanlıqla qarşılayırlar. Heç atamdan soruşmurlar da ki bəs
            indi biz neyləyək, nişanlına, ailəsinə nə cavab verək? Bacıları özləri məsələni yoluna qoyurlar.</p>

        <p>Anam atamı sevməyə başlayanda, onun evlənmək təklifinə "hə" deyəndə bilirdi ki, bu məhəbbət ona nələrin
            bahasına başa gələcək. Moskvadan, əzizlərindən ayrı düşəcək, tanımadığı şəhərdə, üzünü görmədiyi adamların
            arasında yaşayacaq. Ancaq bunun nə qədər çətin olduğunu Bakıya gəlməyə hazırlaşdıqları günlərdə daha yaxşı
            anlayır. Fikir çəkir ki, qürbət yerə gedir, orada neyləyəcək, necə yaşayacaq? Atamın qohumları onu yaxşı
            qarşılayıb, ilk gündən ailənin ən əziz üzvlərindən biri kimi qəbul edəndən sonra ürəklənir. Ziba nənəm,
            bibilərim anama adət-ənənələrimizi, bütün Azərbaycan xörəklərini bişirməyi, tikiş tikməyi öyrədirlər.</p>

        <p>Bir gün atam anamı özüylə Azərbaycan Yazıçılar İttifaqına aparır. Onu Səməd Vurğunla tanış edir. Şair anamı
            böyük səmimiyyətlə qarşılayır, zarafatla deyir: "Məhəbbət Vətənə qalib gəldi!".</p>

        <p>Atam institutda oxuyanda artıq yazıçılıqla ciddi məşğul olur. Bakıya döndükdən sonra, bütün fikrini-zikrini
            yaradıcılığa verir. Ondan olsaydı başqa işə, vəzifəyə vaxt ayırmazdı. Heç zaman dövlət işlərində çalışmaqdan
            xoşu gəlməzdi. Doğrudur, müxtəlif vaxtlarda "Azərbaycan" jurnalında, iki dəfə "Kirpi" jurnalında və başqa
            yerlərdə işlədi, amma getdiyi yerlərdə uzun-uzadı qalmırdı. Yəqin ki, onun əmək kitabçasına yazılan iş stacı
            on-on iki ildən artıq olmazdı. Atama yaxşı vəzifələr də təklif edirdilər, məsuliyyətli iş də tapşırırdılar.
            Bacardığı qədər onlardan imtina edirdi. Ancaq bu həmişə mümkün olmurdu. Məsələn, "Kirpi"yə baş redaktor
            işləməyə getməyə məcbur oldu. Çünki o dövrdə belə məsələlər Mərkəzi Komitə səviyyəsində həll edilirdi. Bəzən
            vəzifələr Kommunist Partiyasının biletini ciblərində gəzdirənlərə dövlət tapşırığı kimi verilirdi. Atam da
            1970-ci ildə "Kirpi" jurnalına baş redaktor təyin olunanda etiraz edə bilmədi. Deyirdi ki, bundan imtina
            etsəm, sonra kitablarımı çap etməyəcəklər. O, vəzifədə də cəmi dörd il işlədi.</p>

        <p>Dövlət işlərindən kənar durmasını özü daha çox yazıçılığıyla bağlayardı: "Nəyimə lazımdır vəzifə?" - deyərdi.
            - "Mənimki odur, çəkilim bir tərəfə, əsərlərimi yazım". Amma mənə elə gəlir, bunun başqa bir səbəbi də
            atamın xarakteriylə bağlıydı. O çox məğrur adam idi. Yaltaqları xoşlamazdı, deyərdi ki, onlar üçün baryer
            yoxdur. Əgər bu gün mövqeyinə görə yaltaqlanırsa, sabah hər şey əlindən çıxanda, sənə asanlıqla da xəyanət
            edərlər. Atamın vəzifə kürsüsündə, mal-dövlətdə gözü yox idi. O, yazıçılığı ən yüksək vəzifə sayırdı. Həmişə
            deyirdi ki, mən elə evdə əsərlərimi yazım, kitablarım vaxtında çap olunsun, qonorarımı alım, bəsimdir. O
            vaxt yazıçılar yaxşı qonorar alırdılar. Bir kitabın puluna ev, maşın almaq olurdı. Yadımdadır, atam "46
            bənövşə" kitabına verilən qonorara özünə avtomaşın aldı.</p>

        <p>1974-cü ildə "Kirpi" jurnalından işdən çıxdı. Ondan sonra çox vəzifələrə dəvət olunsa da, ömrünün sonunadək
            heç yerdə işləmədi. Yaradıcılıqla məşğul olur, qonorarları ilə dolanırdı. O vaxtlar kitab çap etdirmək asan
            məsələ deyildi. Bir neçə nəşriyyat fəaliyyət göstərirdi. Onların da hamısı xüsusi plan əsasında işləyirdi.
            Müxtəlif bölümlər üzrə beşillik tematik plan hazırlanır, Mətbuat Komitəsində baxılır, orada təsdiq olunandan
            sonra çap edilirdi. Atamın əsərləri həmişə plana salınırdı. Ola bilərdi, ya həcmi azaldılır, ya da müxtəlif
            səbəblərə görə ildən-ilə keçirilirdi. Atam kimi yazıçıların əsərlərini plana salmağa məcburuydular. Mən
            Xanbabayev direktor olduğu dövrdə "Yazıçı" nəşriyyatında işləmişəm. O zaman özüm şahidi olmuşam. Hər il
            "dahi rəhbərlər"in - Marksın, Engelsin cild-cild kitablarını çap edirdilər. Onlar nəşriyyata xeyir
            gətirmirdi. Bunu bilə-bilə həmin kitabları dərc edirdilər. Çünki dövlətin planı var idi. Gənc istedadları
            tanıtmaq məqsədilə "Müəllifin ilk kitabı" seriyası ilə buraxılan kitabları, hətta bəzi tanınmış yazıçıların,
            şairlərin kitabları da çox zaman yaxşı satılmırdı. Xeyir gətirən kitabların hər il nəşr edilməsi vacibiydi.
            Salam Qədirzadənin də kitabları ən çox gəlir gətirən kitablardan sayılırdı. Bunu atam olduğuna görə demirəm.
            Onun kitabları heç vaxt rəflərdə yığılıb-qalmırdı. Buna görə də hər dəfə plana atamın da əsərləri
            salınırdı.</p>

        <p>Yaradıcı adamlar, adətən, qısqanc olurlar. Atamın kitablarının mütəmadi nəşr olunması, kitabxanalara
            çatdırılan kimi satılıb-qurtarması kimlərinsə xoşuna gəlmir, bəzən buna görə onun xətrinə dəyirdilər. Atam
            da inciyərdi. O, əvvəlcə Azərbaycan Yazıçılar İttifaqının, sonralar həm də SSRİ Yazıçılar İttifaqının üzvü
            oldu. Ancaq dəfələrlə Azərbaycan Yazıçılar İttifaqı tərəfindən fəxri ad almaq üçün siyahı təqdim ediləndə,
            Mərkəzi Komitədə atamın adı "xeyirxahlarından" birinin sayəsində "ixtisara düşürdü". Əvəzində, layiqli
            adamlarla yanaşı, çox az tanınan adamlara ad verilirdi. Onun "Mükafatın təbəssümündür" adlı bir əsəri var
            idi. Mənə elə gəlir ki, atam kitabına bu adı rəmzi mənada vermişdi. Heç zaman fəxri ad almayan yazıçı komik,
            satirik əsərlərin müəllifi kimi insanlara təbəssüm bəxş etdiyini yada salaraq bildirdi ki, ən böyük mükafatı
            xalqın təbəssümüdür. Hər halda, istər-istəməz üzləşdiyi haqsızlıqları qəlbinə salırdı. Amma dərdini,
            problemlərini heç zaman ailə üzvləriylə bölüşməzdi. Heç anama da yaradıcı aləmdə, ya da iş yerində üzləşdiyi
            çətinlikləri deməzdi. O hətta işdən çıxanda da biz ən axırda bilərdik. Yəqin ki, yaxın dostlarıyla bölüşərdi
            problemlərini. Atam həyatıboyu çox etibarsızlıqlar gördü. Ona görə də dostlarının dairəsini genişləndirməyi
            sevməz, köhnələrin qədrini bilər, yenilərini axtarmazdı. Amma qalanlarla yoldaş münasibəti saxlamağa
            çalışardı. Əli Kərimlə, Əlağa Kürçaylıyla, rejissor Mehdi Məmmədovun qardaşı Teymur Məmmədovla, Nəbi
            Xəzriylə, Qabillə yaxın dost idi. Atam özü dostluqda etibarlı idi. Hərdən bizə elə gələrdi ki, onun üçün
            birinci dostlarıdır, sonra ailəsi. Hərdən anam buna görə ondan inciyərdi də.</p>

        <p>Atam yaxın dostlarının çoxunu tez itirdi. Gənc yaşlarından tanıdığı Əli Kərimin, sonralar isə Əlağa
            Kürçaylının vəfatı ona böyük mənəvi zərbə oldu. Teymur Məmmədovu qardaş qədər sevər, onunla hər gün
            görüşərdi. Teymur əminin itkisi atamı əməlli-başlı sındırdı. Ürəyində bu dünyadan köçmüş əzizlərinin
            dərdləri də vardı. Haqqında danışmaqdan doymadığı sevimli anasının, gənc yaşlarında dünyadan köçmüş
            bacısının, yeganə qardaşının yoxluğunun ağrısını çəkirdi. Amma hətta ən yaxın adamlarını itirəndə də biz
            atamın göz yaşı tökdüyünü, sızladığını görmədik. Anam da həmişə deyir ki, mən onu ən ağır anlarında da
            ağlayan görmədim. Bəlkə də elə ağlayardı ki, kimsə görməsin.</p>

        <p>Müşahidə qabiliyyəti güclüydü. Nəzərindən heç nə yayınmırdı. Bəzən bizə adi görünən, diqqətimizi çəkməyən
            həyat hadisələri barədə o elə maraqla, şirin danışardı ki, gözlərimiz yaşaranadək gülərdik. Atam əsl məclis
            adamıydı, deyib-gülən, zarafatcıl. Çox baməzəydi. Mənə elə gəlir ki, Salam Qədirzadəni zarafatlarsız
            təsəvvür etmək mümkün deyil. Müxtəlif lətifələr düzəldib danışar, eşidənlər uğunardılar. Yadıma onlardan
            biri düşdü: bir gün yolda cavan bir oğlan ona yaxınlaşıb salam verir. Atam salamı alır. Oğlan cibindən
            dəvətnamə çıxarıb ona uzadır: "Xahiş edirəm, mənim toyuma gələsiniz". Atam təbrik edir. Dəvətnaməni alıb
            qoyur cibinə. Evə gəlib oxuyur, onu dəvət edən adamı yadına sala bilmir. Fikirləşir ki, əşi insandır da, nə
            olsun tanımıram, indi dəvət edib, gərək gedəm. O, hər toya da getməzdi. Amma gəncin dəvətini yerə salmayıb
            məclisə gedir. Nə qədər baxırsa, gözünə tanış adam dəymir. Başlayır yeməyə, özünə yüz qram da araq süzür,
            amma görür, tək boğazından keçmir. Ətrafına göz gəzdirir. Özündən sinli, təxminən, 45-50 yaşlı bir kişinin
            də təkcə oturub heç kimə qaynayıb-qarışmadığını görür. Adını bilmədiyindən, hörmət əlaməti olaraq ona
            "Mirzə" deyə müraciət edir: "Mirzə, götür badəni bəylə gəlinin sağlığına içək". İçirlər, bir az keçir, atam
            deyir: "Mirzə, gəl içək gənclərin ata-anasının sağlığına". Beləcə xeyli içirlər. Atam qayıdır ki, ağsaqqal,
            bağışlayın, adınızı da bilmirəm, elə bayaqdan sizə Mirzə deyirəm, adınız nədir? Kişi inciyir: "Yekə kişisən,
            dolamısan məni? Adım elə Mirzədir də".</p>

        <p>Atam saatlarla belə məzəli əhvalatlar danışa bilərdi. Amma ailədə başqa cür idi. Onu məclislərdə görənlər
            inana bilməzdilər ki, bu qədər ciddi, tələbkar ola bilər. Atam həm də çox narahat adamıydı. Bilirdi ki,
            universitetdə dərslərim gündüz saat 1-də bitir, ordan evimizə isə on beş dəqiqəyə gələ bilərəm. Beşcə dəqiqə
            geciksəydim, özünə yer tapa bilməzdi ki, bu qız harda qaldı. Qısqanclığı da vardı. Anamı uzun müddət
            işləməyə qoymadı. O, bütün günü evdə olurdu, heç rəfiqələri də yoxuydu. Elə bibilərimlə rəfiqəlik edərdi.
            Mənə elə gəlir ki, anam qısqanc qadın deyil. Amma kim bilir, bəlkə də çox şeyə bilərəkdən göz yumurdu...</p>

        <p>Atamın bakılılara xas yemək zövqü var idi. Ən çox xəmir xörəklərini xoşlayar, tez-tez anama sifariş verərdi:
            "Qutab hazırla", "Düşbərə bişir"... Anam da can-başla həmin yeməkləri hazırlayardı. O təkcə Azərbaycanın
            adət-ənənələrini, mətbəxini yox, yavaş-yavaş dilimizi də öyrənmişdi. Ruslan məktəb yaşına çatanda onu 190
            saylı orta məktəbə vermişdilər. Anam o zaman çox gənc idi. Səhv edəcəyindən, şəkilçiləri düzgün işlədə
            bilməyəcəyindən ehtiyatlanıb, Azərbaycan dilində danışmırdı. Odur ki qardaşımın oxuduğu məktəbdə
            müəllimlərlə, valideynlərlə ünsiyyət qurmaqda çətinlik çəkirdi. Atamın isə məktəbə tez-tez getməyə vaxtı yox
            idi. Ona görə də anamın xahişini nəzərə alıb, mənim rusca təhsil almağıma icazə verdi. Amma qardaşım da, mən
            də rus dilini mükəmməl bilsək də, öz aramızda yalnız azərbaycanca danışa bilərdik. Atamla da eləcə. Yalnız
            anamla rus dilində danışırdıq. Bu barədə atam ailədə ciddi qayda qoymuşdu. Mən buna görə də atama
            minnətdaram.</p>

        <p>Onun üçün yaradıcılıqla məşğul olmağın müəyyən vaxtı-vədəsi yox idi. Müxtəlif vaxtlarda, daha çox da gecələr
            işləməyi xoşlardı. Səhərlər yuxudan erkən oyanmaqla arası yox idi. O, yazı masasının arxasına keçəndə, evdə
            tam sakitlik hökm sürərdi. Belə vaxtlarda bizə qonaq gəlsəydi, məəttəl qalardı ki, məgər bu evdə uşaq
            yoxdur? İş otağının qabağından barmağımızın ucunda keçərdik. Televizora baxmalı olsaydıq, səsini
            qısardıq.</p>

        <p>Atamın ölümü qəflətən oldu. Dörd il qabaq birinci dəfə insult keçirmişdi. Ancaq özünü həmişə gümrah tutmağa
            çalışdığındanmı, ya onun yoxluğunu ağlımıza gətirmək istəmədiyimizdənmi, bir də belə hadisə baş verəcəyini
            düşünmürdük. 1988-ci ilin 8 noyabrı idi. Qardaşım hərbi yığımdan təzə gəlmişdi. Atam qərara gəldi ki, yenə
            hamımız bir yerə toplaşaq. Görüş yerimizi də təyin etmişdi: "Əfsanə" kafesində xudmani bir məclis
            qurulacaqdı. Həmin gün avtomaşınını təmirə aparıb. Durduğu yerdəcə onu ikinci dəfə insult vurub. "Semaşko"
            xəstəxanasına aparıblar. Vəziyyəti ağır olduğundan anamdan başqa heç kimi yanına buraxmırdılar. Xəstəxanaya
            aparıldığının səhəri özünə gəlib. Olub-keçənləri anlayıb. Bir neçə saat danışıb. Sonra yenidən - üçüncü dəfə
            insult keçirib. Komaya düşdü. Bundan sonra bir də özünə gəlmədi. Bir həftə sonra - noyabrın 15-də atam 64
            yaşında dünyasını dəyişdi.</p>

        <p>Onu, əlbəttə, həmişə ilk növbədə bizi sevən, isti nəfəsi, qayğısı ilə rahatlıq tapdığımız atamız kimi
            xatırlayıram. Kişilərin bir çoxu kimi övladlarına qarşı çox tələbkar olsa da, sonralar nəvələrinə qarşı daha
            yumşaq, mülayim, mehriban baba oldu. Sonra nəzərimdə yazıçı Salam Qədirzadə kimi canlanır. Qardaşımla mən
            ona əsərləri barədə düşündüklərimizi deyir, bəzən hətta tənqid də edirdik. Diqqətlə bizə qulaq asardı.
            Atamla zövqlərimiz bir-birinə çox bənzəyərdi. Hətta eyni yazıçıları, eyni əsərləri sevərdik. Onun özünün
            satirik əsərlərindən xoşu gələrdi, mənim də. Bir də söz yox ki, "Kəndimizdə bir gözəl var" əsərini çox
            sevirəm. Yəqin, ona görə ki, atam mənim adımı o əsərdəki qıza verib.</p>

        <p>Atamla fəxr edirdim. Orta məktəbin xüsusilə yuxarı siniflərində, Bakı Dövlət Universitetində oxuduğum illərdə
            ətrafımdakı həmyaşıdlarımın onun əsərlərinə olan maraqlarını, kitablarının əldən-ələ gəzdiyini görür,
            fərəhlənirdim. Onun adı çox yerdə köməyimə çatıb. Mən ailə quranda atam həyat yoldaşımdan xahiş etdi: "İcazə
            ver, Leyla öz soyadını dəyişməsin". O da atamın sözünü yerə salmadı. Mən bugünədək Qədirzadə soyadını
            daşıyıram. Onun vəfatından illər keçsə də, soyadımı görənlər "Siz yazıçı Salam Qədirzadənin qızısınız?" -
            deyə soruşur, müsbət cavab alanda hörmətlə yanaşır, bacardıqları qədər kömək göstərirlər.</p>

        <p>Tale ona sonuncu əsərini də tamamlamağa fürsət verdi. Heç bir yarımçıq işi qalmadı. Son vaxtlar nasaz olduğu
            üçün az yazırdı. Axırıncı əsəri "Hər gün ömürdən gedir" nəşriyyatda idi. Onun çapını görməyə tələsirdi. Atam
            optimist adam idi. Kitablarına da xarakterinə uyğun adlar verərdi. Yəqin, buna görə şair Sabir Rüstəmxanlı
            tematik plana salınarkən təəccüblə mənə dedi: "Nə pessimist adı var atanızın bu kitabının, çox gözəl addır,
            amma kədər doğurur. Salam müəllim özü şən adamdır axı, ondan nə əcəb kitabına bu cür ad qoyub?". Bilmirəm,
            yəqin, ömrünün sonuna yaxınlaşdığını duyurdu. Ona görə kitabını "Hər gün ömürdən gedir" adlandırmışdı.
            Həmişə heyfsilənirəm ki, o kitabın çapını görmədi. Həmin sətirləri biz təəssüf hissiylə onun məzardaşına
            yazdırdıq.</p>

        <p>Təəssüflənirəm ki, indi ədəbiyyata sevgi azalıb. Bədii əsərləri oxumaq istəmirlər. Amma hələ də yazıçı Salam
            Qədirzadənin əsərlərini, az qala, əzbər bilən, onun kitabları ilə tərbiyə alan böyük bir nəsil var.</p>

        <div class="post-date">
            <div class="dots">...</div>
            <p>Mədəniyyət</p>
            <div><span class="date">2013</span></div>
        </div>
    </div>
</article>