<article class="press_post">
    <h2>Sovet dövrünün populyar yazarlarından olan Salam Qədirzadənin oğlu ilə söhbət.</h2>

    <div class="author paragraphs_block">
        <mark>Hüsü Hacıyev küçəsində bir bina var: yazıçıların binası...</mark>
        <p>
            Oranı hamı tanıyır; qəsdən, ya da elə maraq xətrinə kimə yaxınlaşıramsa, bir ağızdan nişan verirlər.
            İkinci mərtəbəyə qalxanda görürəm, qapının üstündəki “Salam Qədirzadə” adı hələ də durur.</p>

        <p>
            Kimlər yaşamayıblar bu binada. O kimlərin yaxınları, varisləri ilə söhbətim çox olub; elliklə bu binanın
            qaynar çağlarını nisgillə xatırlayıblar.Elə Salam Qədirzadənin oğlu da söhbət boyu atasının gecə yarısınacan
            çəkən
            qonaqlıqlarından, ədəbi mübahisələrindən, nərdtaxta oynamaqlarından ağız dolusu danışdı.
        </p>
    </div>

    <div class="author paragraphs_block">
        <mark>“ - Salam Qədirzadənin özü də unudulub, yaradıcılığı da...”</mark>
        <p>
            Söhbətin bu giley-güzar dolu cümlə ilə başlamağı heç ürəyimcə deyil. Di gə, günah atası haqqında xatirələrə
            baş vuran oğulda – Ruslan Qədirzadədə deyil. Günah məndədir – nəfəs dərər-dərməz “deyirlər, vaxtilə o, çox
            məşhur olub, indi... indisini bəlkə siz danışasınız... ” deyirəm. Danışır...
        </p>
    </div>

    <div class="comment paragraphs_block">
        <mark>“Qapımızı döyən, hal-əhval tutan yoxdur”</mark>
        <p>
            - Sovet dövründə əsərləri iki-üç ildən bir nəşr olunurdu. 98-ci ildən sonra atamın bir cümləsi belə çap
            olunmayıb
        </p>

        <p>1998-ci ildə “Gənclik” nəşriyyatının direktoru zəng vurdu ki, Ruslan, gəl yanıma, səninlə söhbətim var.
            Görüşdük dedi, Salam Qədirzadənin kitabını nəşr etmək istəyirik, amma pulumuz yoxdur, imkanınız varsa, kitab
            sizin hesabınıza çıxsın. Razılaşdım, 1000 tirajına 300-400 dollar xərc çəkdik, “Qış gecəsi” romanı atamın ad
            günü ərəfəsində çap olundu. </p>

        <p>
            Sən gəlməmişdən əvvəl qızımla söhbət edirdim, dedi babamın xatirə lövhəsi məsələsini danış, bəlkə bir
            köməkliyi oldu. Təxminən 14-15 il əvvəl bacım dedi, Ruslan, bu binada yaşayan əksər yazıçıların xatirə
            lövhəsi var, atamızdan başqa. Bu məsələ məni çoxdan narahat eləyirdi.Əslində, bu xatirə lövhəsini mən özüm
            vurdura bilərəm, amma bu, qanunsuz olur, gərək Nazirlər Kabinetinin icazəsi olsun.Bacımla üç səhifəlik bir
            məktub hazırladıq, yollandım Yazıçılar Birliyinə. Çingiz Abdullayev Anar müəllimin yanında idi. Anar müəllim
            məni uşaq vaxtı görmüşdü, tanımadı.</p>

        <p>Biləndə ki Salam Qədirzadənin oğluyam, hər ikisi şad oldular, isti münasibət göstərdilər. Dedim Anar müəllim,
            mən heç vaxt bura xahiş- minnət üçün gəlməmişəm, dolanışığımız da yaxşıdır, pis yaşamırıq. İstəyirik, atamın
            xatirə lövhəsi yaşadığı binaya vurulsun və mümkünsə, doğulduğu Maştağa kəndində onun adına bir küçə
            verilsin. Söz verdi, arxayın saldı ki, bu işlə məşğul olacağam. Üstündən 6-7 ay keçdi, cavab gəlmədi. O vaxt
            Bəxtiyar Vahabzadə adlar komissiyasının üzvü idi, bir də zəng edəndə Anar müəllim dedi, Bəxtiyar müəllimlə
            danışacağam, sizə qonşu olub, kömək edəcək. Üç il keçdi yenə cavab gəlmədi. Bir də zəng vurub xatırlatdım,
            cavab verdi ki, bilirəm oğlum, çalışacağam sizə kömək edim. Atamın yubileyi Yazıçılar Birliyində keçiriləndə
            gileyləndim. Məclisi atamın yaxın dostu, gözəl şairimiz Qabil aparırdı. Orda da söz verdilər ki, düzələn
            işdir, amma...</p>

        <p>
            Bizim binamızda iki adamın xatirə lövhəsi yoxdur: atamla Əliağa Kürçaylının. Salyanda Əliağa Kürçaylının
            büstü qoyulub, adına küçə, məktəb var.
            Bircə mənim atamın xatirəsinə etinasızlıq göstərildi. Bu münasibət mənə çox ağır gəlir. Qeyri-qanuni olsa
            da, xərcini çəkib, xatirə lövhəsini özüm vurmağa məcburam. Dostlarından indi heç kim qalmayıb, qapımızı
            döyən, hal-əhval tutan yoxdur. Bilirsən, get-gəlli olan ailə üçün bunları yaşamaq çox çətindir. Sağ
            olsunlar, televiziyalar – AzTV, ANS, XƏZƏR TV atamın xatirəsini həmişə diqqətdə saxlayıblar. ANS ayrıca
            veriliş etmək istəyir, görək sağlıq olsun...
        </p>
    </div>

    <div class="comment paragraphs_block">
        <mark>“O qız atamı çox sevirdi...”</mark>
        <p>
            - Sağlığında atamı hamı tanıyırdı. Maştağadan yeniyetmə vaxtlarında çıxıb, amma dostları ilə bibimin bağ
            evinə getməyi sevərdi. Nərdtaxta oynayırdılar, yeyib-içirdilər, bir də gördün, bir kənara çəkilib yazırdı.
        </p>

        <p>
            Maştağada uşaqdan böyüyə kim eşidirdisə Salam Qədirzadə gəlib, mütləq gəlib görüşür, hal-əhval tuturdular.
            Atamla dəfələrlə toylarda olmuşam, adamlar barmaqla göstərirdilər ki, bax, “Qış gecəsi”ni yazan Salam
            Qədirzadə budur.
        </p>

        <p>
            Atamgil ailədə iki qardaş, beş bacı olublar. Dadaş babamı mən görməmişəm, amma atam danışardı ki, kiçik
            ticarətlə məşğul olub. Mənim nə əmim, nə bibilərim öz təvəllüd tarixini dəqiq bilməyiblər.
            Bir dəfə soruşdum ki, ata, sən doğum tarixini belə dəqiqliklə necə bilirsən? Sən demə, Dadaş babam arzu
            edirmiş ki, birinci uşağı oğlan olsun. Bibim doğulandan sonra deyəsən, kişinin qanı bir az qaralıb.
            Pirşağının yaxınlığında Ləhiş bağları deyilən yer var, orda evləri olub. Atam anadan olanda babam o qədər
            sevinib ki, həyətdəki çinar ağacının başına dırmaşıb, bıçaqla 23.10.04 yazıb. Yəni 1923-cü il, dördüncü ay,
            onuncu gün. Atam qırx yaşınacan o yazı, ağac dururdu. Sonra bağı satdılar.
        </p>

        <p>Anam milliyətcə rusdur. Tanışlıqlarının qəribə tarixçəsi var. Atam 52-ci ildə Moskvada Ədəbiyyat İnstitutunda
            İsa Hüseynov, Nəbi Xəzri ilə bir yerdə oxuyub, yataqxanada eyni otaqda qalıblar. Atam danışırdı ki,
            yataqxana Peredelkinoda yerləşirmiş, elektrik qatarı ilə təxminən saatyarımlıq yol gedirmişlər. Qatarda üç
            nəfər qız həmişə eyni vaqonda gedirmişlər. Qızlardan ikisi deyib gülürmüşlər, biri həmişə sakit oturar,
            kitab oxuyarmış. Özü də qızılsaç, yaraşıqlı rus qızı olub. Atam qızla tanış olub, bir-birini seviblər.
            Moskvada balaca bir kafedə özlərinə toy ediblər.</p>

        <p>Toy günü eşidiblər ki, Səməd Vurğun Moskvadadır, deyiblər, onu dəvət edək. Atam danışırdı ki, Səməd Vurğun o
            vaxt azərbaycanlı tələbələrə tez-tez baş çəkər, ciblərinə pul qoyar, əyər-əskikləri ilə maraqlanarmış. Toyda
            atam anamı Səməd Vurğuna təqdim edib ki, Roza ilə evlənirik. Səməd müəllim soruşub, səninlə Azərbaycana
            getməyə razıdır? Anam deyib, əlbəttə. Bu Səməd Vurğunun xoşuna gəlib, “onda məhəbbət vətənə qalib gəldi?”
            soruşanda anam “Salamla hər yerə getməyə hazıram” deyib.</p>

        <p>53-cü ildə mən, 58-ci ildə bacım doğulub. Maraqlı orasıdır ki, atam Moskvaya getməmişdən qabaq Bakıda
            sevgilisi olub. Özü də, atamın oxucusuymuş. Atam oxumağa gedəndən sonra qızı unudub.</p>

        <p>İndi o qadını tanıyıram, 80-dən yuxarı yaşı var. Bibim həmişə danışardı ki, o qız atanı çox sevirdi. Sonralar
            atama əsərlərinə görə 500-ə yaxın məktub gəlmişdi. Çoxu da xanım oxuculardan olardı. Amma anam bunu həmişə
            normal qarşılayıb, aralarında narazılıq olmayıb. Anam bilirdi ki, həyatını yazıçıya bağlayıb.</p>
    </div>

    <div class="comment paragraphs_block">
        <mark>“Heydər Əliyev atama deyib ki...”</mark>
        <p>
            - Seyfəddin Dağlı “Kirpi” jurnalının baş redaktoru olanda atam işdən öz xoşu ilə çıxıb. Uzun müddət
            işləmədi, ancaq evdə oturub yazmaq istəyirdi. 76-cı ildə telefon zəng çaldı, cavab verdim. Bir nəfər rus
            dilində dedi ki, mən Heydər Əliyevin köməkçisiyəm, Salam Dadaşoviçlə danışmaq istəyirəm.

            Atam Mehdi Məmmədovun qardaşını çağırıb dedi, məni Mərkəzi Komitəyə çağırırlar. İki-üç gün sonra görüşə
            getdi. Bizə dedi, Heydər Əliyev məni “Kirpi” jurnalına baş redaktor təyin etmək istəyir.

            Səhhətində problem olduğuna görə qəbul etməmişdi ki, “Kirpi”də işləmək çətindir. Heydər Əliyev deyib, gedin
            fikirləşin, Süleyman Rüstəm sizdən başqa heç kimin məsləhət görməyib.

            İkinci görüşdə atam yenə imtina edəndə Heydər Əliyev deyib qəbul etməlisiniz, bu birinci katib kimi mənim
            təkidimdir. O vaxt “Kirpi” Mərkəzi Komitənin orqanı idi və birbaşa birinci katibə tabe idi.

            Təsəvvür edin ki, bu vəzifə komitənin şöbə müdiri səviyyəsində vəzifə olub: xidməti maşın, xüsusi xəstəxana,
            xüsusi istirahət evləri...

            Dörd il baş redaktor oldu, sonra təzyiqlərə, xahiş zənglərinə görə öz ərizəsi ilə işdən çıxdı.

            Vəli Axundovun rəhbərliyi dövründə də vəzifədən imtina etmişdi. Vəli Axundov Şıxəli Qurbanovu Mərkəzi
            Komitəyə ideologiya katibi təyin edəndən bir ay sonra həyətimizdə qara Volqa dayandı, Şıxəli müəllim
            maşından düşüb bizə nahar yeməyinə gəlmişdi. Sonralar mən öyrəndim ki, Şıxəli əmi atama təbliğat və təşviqat
            şöbəsinin müdiri vəzifəsini təklif edib, atam razılaşmayıb ki, Şıxəli, mən vəzifə adamı deyiləm, işləyə
            bilmərəm, qan təzyiqim məni əldən salıb, əsəblərim yerində deyil.
        </p>
    </div>

    <div class="comment paragraphs_block">
        <mark>“Qış gecəsi” romanına görə bir neçə sevgilim vardı...”</mark>
        <p>
            - Atam gecələr yazardı. Öz külqabısı, armudu stəkanı, qənqabısı vardı. Gecə üçə-dördə qədər yazardı. Yazanda
            siqareti çox çəkərdi, otaq tüstü-dumana bürünərdi. Təzə nəsə yazandan sonra Qabil başda olmaqla dostlarını
            başına yığıb oxuyar, müzakirə edərdilər. Bizə icazə verməzdi ki, yazı yazdığı otağına girək. Bu evi atama
            56-cı ildə veriblər. Bura köçənə qədər biz Nərimanov heykəlinin yanındakı binada Ziya Bünyadovla qonşu
            olmuşuq. Birotaqlı ev darısqal olduğundan bu evi Yazıçılar İttifaqı verib.
            “Qış gecəsi” romanı atama böyük şöhrət gətirdi. Mən hər yerdə Salam Qədirzadənin oğlu olduğumu gizlətməyə
            çalışırdım, amma olmurdu. Cavan olanda yazıçı oğlu olduğuma, “Qış gecəsi” romanına görə bir neçə sevgilim
            vardı, evə-eşiyə yığışmırdım (gülür).
            Özü də o vaxt heç nədən korluğumuz olmurdu, dolanışığımız yaxşı idi. Axırda atam özü dilləndi ki, 27 yaşın
            var, bəsdir, evlənmək vaxtıdır.
            Anamla bir yerdə bir qız işləyirdi, atama məsləhət elədi, atam da dedi, sən allah kimi istəyirsən al, bunun
            başını bağlayaq. Bəlkə evə gələn zənglər kəsilər.
            Bizi tanış etdilər, evləndik. Sonra məni əsgər çağırdılar, zabit olanda yoldaşımı da özümlə apardım
            Odessaya.
            İndi Azərbaycan Stolüstü Tennis Federasiyasında mətbuat katibiyəm.
            Atamın səhhətinə qarşı laqeydliyi vardı. Siqareti çox çəkirdi, əsəbliydi.
            Bir dəfə insult keçirdi, həkim ona qadağalar qoymuşdu. 1987-ci ilin noyabrın 16-da insultdan dünyasını
            dəyişdi...
        </p>
    </div>

    <div class="author">
        <p>Ruhu şad olsun!</p>
    </div>

    <div class="comment">
        <div class="post-date">
            <div class="dots">...</div>
            <p>Cavid Zeynallı azadliq.org</p>

            <div><span class="date">15 Oktyabr 2012</span></div>
        </div>
    </div>
</article>