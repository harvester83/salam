<article class="press_post">
    <h2>"Hər gün ömürdən gedir"</h2>

    <div class="author">
        <mark>Salam Qədirzadə kitablarına xarakterinə uyğun adlar verərmiş</mark>
        <p>
            İndinin yox, yaxın keçmişin gənclərinin çox sevdiyi bir yazıçı vardı. Məhəbbət, sevgi, həyat dolu romanları,
            povestləri, hekayələri gənclərin əlindən, dilindən düşməzdi. O kitablarla böyüyüb tərbiyə alan böyük bir
            nəsil
            var. Hələ də o əsərlərin təmizlik, paklıq ruhu insanları tərk etməyib. Salam Qədirzadənin əsərlərində
            zamanının
            eyiblərinə güzgü tutan satira da, komiklik də vardı. Bu əsərlərin hər biri təəssüf ki, kitaba qiymət
            verməyən
            indiki nəslin yox, o zamankı nəslin stolüstü kitabına çevrilmişdi.
        </p>
    </div>

    <div class="comment paragraphs_block">
        <mark>Rəssam, yoxsa yazıçı?</mark>
        <p>Salam Qədirzadə sadə və zəhmətkeş bir ailədə dünyaya gəlib. Həyatın sərt üzünü hələ uşaq ikən görüb. Atası
            ailəni balaca dükanından qazandığı gəlirlə dolandırır, anası Ziba isə evdə dərzilik edirmiş. Yeddi nəfərdən
            ibarət külfəti dolandırmaq o qədər asan deyildi. "Atam evin ikinci uşağı idi. Onun ailələrində xüsusi yeri
            olub. Qardaş-bacıları onu həddindən artıq çox istəyib, böyük qardaşlarıyla fəxr ediblər" - deyə qızı Leyla
            xanım danışır: "Evdə onun oxuması, yaradıcılıqla məşğul olması üçün hər cür şərait yaradılıb. Nəslimizdə
            onun qədər yazıçılığa heç kim həvəs göstərməyib. Atam orta məktəbdə oxuyanda şeirlər yazırmış. Ancaq o,
            təkcə ədəbiyyata yox, rəssamlığa da həvəs göstərib. Yaxşı şəkillər çəkərmiş. Hələ musiqiyə, tar çalmağa olan
            həvəsini demirəm. Musiqi təhsili almaq üçün sənədlərini musiqi məktəbinə də verib. Məktəbə qəbul prosesində,
            uşaqların musiqi qabiliyyətlərinin yoxlanmasında dahi Üzeyir bəy də iştirak edirmiş. Atam dahi bəstəkar
            qarşısında tarda bir musiqi ifa edib. Üzeyir bəy qulaq asandan sonra: "Bala, sən bu sənətdən uzaqlaş, tarzən
            olmaq sənlik deyil" - deyə atama məsləhət verib. Atam sonralar ona doğru yol göstərdiyi üçün Üzeyir bəyin
            ruhuna dua oxuyardı.</p>

        <p>Musiqiçi olmaq həvəsindən əl çəkəndən sonra atam seçim qarşısında qalıb: rəssam, yoxsa yazıçı olsun?
            Şeirləri pis deyildi, oxuyanlar bəyənirmiş. Ədəbiyyata və poeziyaya olan həvəsi digər maraqlarını üstələyir
            və o, yazıçı olmağı qərara alır.</p>

        <p>Böyük Vətən müharibəsinin başlaması onsuz da çətinliklə yaşayan ailələri daha da ehtiyacla üzləşdirib. Atam
            könüllü olaraq cəbhəyə yollanıb. Müharibədə rabitəçi kimi dəfələrlə ölümlə üz-üzə gəlib. Bir neçə dəfə
            yaralanıb.</p>

        <p>Müharibə qurtarandan sonra Bakıya qayıdıb. Oxuyub təhsil almaq arzusunda olan atam yenidən ailənin
            problemlərinə kömək etməli olub.</p>
    </div>

    <div class="comment paragraphs_block">
        <mark>"Məhəbbət Vətənə qalib gəldi!"</mark>
        <p>"Oxumaq həvəsi atamı bir an da tərk etməyib. O, gecəsini-gündüzünə qataraq hazırlaşıb və Maksim Qorki adına
            Ədəbiyyat İnstitutuna daxil olub" - deyə Leyla xanım söhbətinə davam edir.</p>

        <p>- Moskvada oxuyarkən Nəbi Xəzri, Əli Kərim, İsa Hüseynovla dost olub, yataqxanada eyni otaqda yaşayıblar.
            Tələbəlik illərində çox kasıblıqla dolanıblar. Atam danışırdı ki, onlar çətin günün dostu olublar. Ac
            vaxtlarında Moskva yeməkxanalarında stolların üstünə qoyulan pulsuz çörək və xardalı yeyirmişlər. Həmin
            illərdə Səməd Vurğunun onlara köməyi dəyib. O, azərbaycanlı tələbələrin dərd-sərləri ilə maraqlanar,
            restorana aparıb onlara qonaqlıq verərmiş. Bu tələbələrin korluq çəkdiyini görən şair bəzən soyuq qış
            aylarında birinə şərf, birinə papağını bağışlayıb. Atam şairin bu xeyirxahlığını həmişə minnətdarlıqla
            xatırlayardı.</p>

        <p>Atamgil institutda oxuyanda yazıçı Fadeyevin Moskva kənarındakı bağ evində qalıblar. Yataqxana kimi
            tələbələrin istifadəsinə verilən bağ evini onlar əsl yaradıcılıq evinə çeviriblər. Azərbaycanlı tələbələr
            hər gün bağ evindən şəhərə elektrik qatarıyla gedib-gəlirmişlər. Atamgil hər səhər qatarda qəşəng, sarışın
            bir qızla yol-yoldaşı olublar. Tanışlıq zamanı Roza adlı gənc qız onların Ədəbiyyat İnstitutunda təhsil
            aldıqlarını biləndə sevinib. O da ədəbiyyatı həddindən artıq sevdiyini söyləyib. O gündən həmin elektrik
            qatarı atamgilin ədəbiyyat barədə müzakirə məkanına dönüb. </p>

        <p>Bir gün atam qıza vurulduğunu anlayıb. Cürət edib ürəyini ona açandan sonra qızın da ona meyili olduğunu
            öyrənib. Amma atam Bakıda heykəltəraş qızla nişanlı olduğunu ondan gizlədib.</p>

        <p>Tələbəlik illəri başa çatandan sonra atam qızdan ayrılmaq istəməyib. Xudmani bir məclisə yoldaşlarını
            yığaraq onunla evlənib. Atamın Moskvadan gözəl bir qızla dönməsi hamını çaşdırıb. Ancaq özlərinə ağsaqqal
            bildikləri atama cürət edib heç kəs bir söz deyə bilməyib. Gəlinə "Salamın nişanlısı var" - deməyə də heç
            kəsin ürəyi gəlməyib. Beləcə, moskvalı qız bu evin əziz-xələf sakininə çevrilib. Əlbəttə, qərib şəhərdə,
            tanımadığı adamların arasında olmaq anam üçün asan deyildi. Ancaq onun məhəbbəti hər şeyi üstələmişdi. Bir
            də qohum-əqrəba da onu mehribanlıqla qarşılamışdı. Nənəm, atamın bacıları anama adət-ənənələrimizi, bütün
            Azərbaycan xörəklərini bişirməyi öyrədirlər. Anam tikiş tikməyi də həvəslə öyrənir.</p>

        <p> Bir gün atam anamı özüylə Yazıçılar İttifaqına apararaq Səməd Vurğunla da tanış edir. Şair anamı
            mehribanlıqla qarşılayır: "Məhəbbət Vətənə qalib gəldi!" - deyə zarafatından da qalmır.</p>
        </p>
    </div>

    <div class="comment">
        <mark>"Vəzifə nəyimə lazımdır?"</mark>
        <p>Salam Qədirzadə Bakıya qayıtdıqdan sonra bütün fikrini yazıçılığa verir, kitablarını çap etdirir. Dövlət
            işlərinə maraq göstərmir. Ona yaxşı vəzifələr təklif etsələr də, bacardığı qədər bu işlərdən imtina edir. O,
            yazıçılığı ən yüksək vəzifə sayır. "Atamın vəzifə kürsüsündə gözü yox idi. Ancaq vəzifələrdən imtina etmək
            də həmişə mümkün olmurdu. Müxtəlif vaxtlarda "Azərbaycan" yurnalında, ilk dəfə "Kirpi" yurnalında və başqa
            yerlərdə işlədi, amma getdiyi yerlərdə uzun-uzadı qalmadı. "Kirpi"də baş redaktor işləməyə məcbur oldu.
            Çünki o dövrdə belə vəzifələr Mərkəzi Komitə səviyyəsində həll edilirdi. Bəzən vəzifələr dövlət tapşırığı
            kimi verilirdi. Atam da 1970-ci ildə "Kirpi" yurnalına baş redaktor təyin ediləndə yaxasını kənara çəkə
            bilmədi. Deyirdi ki, bu vəzifədən imtina etsəm, sonra kitablarımı çap etməyəcəklər. Ancaq o, bu vəzifədə
            cəmi dörd il işlədi. "Vəzifə nəyimə lazımdır? Yazıçının vəzifəsi odur ki, çəkilsin bir tərəfə, əsərlərini
            yazsın" - deyə həmişə söyləyirdi. "O vaxt yazıçılar yaxşı qonorar alırdılar. Bir kitabın puluna ev, maşın
            almaq olurdu. Atam "46 bənövşə" kitabından qazandığı qonorarla özünə maşın aldı. Atam dörd il işlədikdən
            sonra "Kirpi" yurnalından işdən çıxdı. Ondan sonra çox vəzifələrə dəvət olunsa da, ömrünün sonunadək heç
            yerdə işləmədi. Yaradıcılıqla məşğul olur, qonorarları ilə dolanırdı. O vaxtlar kitab çap etdirmək asan
            məsələ deyildi. Atamın əsərləri həmişə plana salınırdı. Çünki atam kimi yazıçıların əsərlərini plana salmağa
            məcburuydular. "Xeyir gətirən kitabların isə hər il nəşr edilməsi vacibiydi. Atamın kitabları da ən çox
            gəlir gətirən kitablar sırasına daxil idi. Bu kitablar heç vaxt rəflərdə yığılıb qalmırdı".
        </p>
    </div>

    <div class="author paragraphs_block">
        <mark>Etibarlı dost</mark>
        <p>Leyla xanım danışır ki, Salam Qədirzadə övladları üçün qayğıkeş ata, ailəsinə can yandıran bir insan olub.
            Övladlarına qarşı tələbkar olması uşaqlarının gələcək tərbiyəsində və təhsilində mühüm rol oynayıb. Oğlu
            Ruslan da, qızı Leyla da həmişə ataları ilə fəxr ediblər:</p>
    </div>

    <div class="comment paragraphs_block">
        <p>
            - Qardaşımla mən ona əsərləri haqqında fikirlərimizi deyir, bəzən tənqid də edirdik. O bizə diqqətlə qulaq
            asar, fikrimizə hörmətlə yanaşırdı. O istəyirdi ki, bizdə müstəqil fikir formalaşsın. Atamla zövqlərimiz
            bir-birinə bənzəyirdi. Hətta eyni yazıçıları, eyni əsərləri sevərdik. Atam öz satirik əsərlərini xoşlayırdı.
            Biz də həmin əsərləri sevirdik. Bir də mən onun bütün əsərləri içərisində baş qəhrəmanını mənim adımla
            adlandırdığı "Kəndimizdə bir gözəl var" əsərini çox sevirəm. Əlbəttə ki, mən həmin əsərdə həyatından bəhs
            edilən qız deyiləm. Sadəcə, atam həmin əsəri mənə ithaf edib. Eləcə də "Su pərisi Mariana" əsəri anam Roza
            xanıma, "Sevdasız aylar" isə qardaşım Ruslana həsr olunub.
            Orta məktəbdə və universitetdə oxuduğum illərdə tələbələrin atamın kitablarına olan maraqlarını görüb bir
            övlad kimi fərəhlənirdim. O dövrdə "46 bənövşə", "Kəndimizdə bir gözəl var", "Sevdasız aylar" kitabları
            gənclərin ən çox sevdiyi əsərlər idi.
        </p>
    </div>

    <div class="comment paragraphs_block">
        <mark>Unudulan yazıçı</mark>
        <p>
            Yaradıcı adamlar arasında bütün dövrlərə xas olan qısqanclıq və paxıllıq hissləri Salam Qədirzadədən də yan
            ötməyib. Kitablarının çap olunmaq üçün hər il plana salınması, mütəmadi nəşr olunması, kitabxanalara
            çatdırılan kimi satılıb qurtarması kimlərdəsə qısqanclıq yaradıb. Əvvəlcə Yazıçılar İttifaqının, sonralar
            SSRİ Yazıçılar İttifaqının üzvü olan yazıçının dəfələrlə Azərbaycan Yazıçılar İttifaqı tərəfindən fəxri ad
            almaq üçün namizədliyi irəli sürülsə də, adı Mərkəzi Komitədə siyahıdan çıxarılıb. Onun fəxri adı
            başqalarına verilir. "Atam "Mükafatım təbəssümümdür" əsərinə adı rəmzi mənada vermişdi. Fəxri adı
            saxtakarlıqla başqalarına verilən atam satirik əsərlərin müəllifi kimi insanlara təbəssüm bəxş etdiyindən
            qürur duyduğunu, ən böyük mükafatı xalqın təbəssümü olduğunu yazırdı. Ancaq o bizə hiss etdirməsə də,
            qarşılaşdığı haqsızlıqlardan inciyirdi. Atam işdə qarşılaşdığı problemləri evdə açıb-ağartmağı xoşlamırdı.
            İşlə bağlı hansısa problemi olduğunu biz sonralar eşidərdik.
        </p>
    </div>

    <div class="author paragraphs_block">
        <p>Salam Qədirzadəni qəfil ölüm yaxalayır. Bir neçə il əvvəl insult keçirsə də, özünü xəstəliyə təslim etmək
            istəmirmiş. Həmişə deyib-gülən, şən, zarafatcıl bir insanı xəstəhal təsəvvür eləmək nəinki ailələri, bütün
            dostları və qohumları üçün çətin imiş. Ancaq 1988-ci ilin 8 noyabrı... Hərbi toplantıdan təzə qayıdan oğlu
            Ruslanın gəlişi münasibəti ilə "Əfsanə" kafesində xudmani bir məclis təşkil etməyi qərara alan yazıçı həmin
            gün durduğu yerdə ikinci dəfə insult keçirir. "Atamı "Semaşko" xəstəxanasına aparırlar. Vəziyyəti ağır
            olduğu üçün anamdan başqa heç kimi yanına buraxmırdılar" - deyə Leyla xanım xatırlayır: </p>
    </div>

    <div class="comment paragraphs_block">
        <p>
            - Atam yalnız səhəri gün özünə gəlib, başına gələnləri anlayıb. Bir az söhbət də edib. Sonra yenidən, üçüncü
            dəfə insult keçirib. Bir həftə koma vəziyyətində yaşadıqdan sonra vəfat etdi. Dünyasını dəyişəndə atamın 64
            yaşı vardı.
        </p>
    </div>

    <div class="author paragraphs_block">
        <p>Salam Qədirzadənin bəxti onda gətirmişdi ki, öləcəyini duyurmuş kimi, bütün yarımçıq işlərinə əl gəzdirmiş,
            hamısını tamamlamışdı. Nəşriyyatda çapa hazırlanan "Hər gün ömürdən gedir" adlı sonuncu əsərinin çapını
            görməyə tələsirmiş. Optimist adam olduğundan kitablarına da xarakterinə uyğun adlar verərmiş. Bəlkə də
            ömrünün sona yaxınlaşdığını duyduğundan axırıncı kitabını "Hər gün ömürdən gedir" adlandırır. Ancaq kitabın
            çapını görmək ona qismət olmur. Övladları həmişə buna görə heyifsilənirlər. Buna görə də həmin sətirləri
            böyük təəssüf hissiylə atalarının başdaşına yazdırıblar. </p>

        <p>Leyla xanım atasının ölümündən 20 ildən çox vaxt keçməsinə baxmayaraq, xatirəsinin yad edilmədiyindən,
            yaşadığı binaya xatirə lövhəsinin vurulmadığından gileylənir. Bununla bağlı onların əlaqədar ünvanlara
            müraciətləri cavabsız qalıb. Salam Qədirzadənin "Qış gecəsi" romanı hələ sağlığında çap olunmaq üçün plana
            salınsa da, ölümündən sonra heç kəs bu əsərlə maraqlanmayıb. Övladları həmin kitabı öz hesablarına çap
            etdiriblər: </p>
    </div>

    <div class="comment paragraphs_block">
        <p>
            "Görünür, biz də ona oxşamışıq. Qapılar döyməyi xoşlamırıq. Ona görə də atamın xatirəsini əbədiləşdirməyə,
            doğum və ya ölüm günlərində bir xatirə gecəsi keçirməyə nail ola bilmirik..."
        </p>
    </div>

    <div class="comment">
        <div class="post-date">
            <div class="dots">...</div>
            <p>Təranə MƏHƏRRƏMOVA Kaspi S.14.</p>
            <div><span class="date">10 aprel 2009</span></div>
        </div>
    </div>
</article>