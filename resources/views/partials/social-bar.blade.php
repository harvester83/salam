<div class="socium">
    <ul>
        <li title="Facebook" class="socium-first"><a href="https://www.facebook.com/SalamQadirzade" target="_blank">Facebook</a></li>
        <li title="Youtube" ><a href="https://www.youtube.com/user/SalamGadirzadeh" target="_blank">Youtube </a></li>
        <li title="<?php echo trans('common.contact'); ?>" ><a href="mailto:info@salamqadirzade.com"><?php echo trans('common.contact'); ?></a></li>
    </ul>
</div>