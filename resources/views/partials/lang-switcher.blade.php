<?php
    $urlPath = Request::path();
    $urlPath = str_replace(app()->getLocale(), '', $urlPath);

    $languages = [
        'az' => 'aze',
        'ru' => 'рус',
//        'en' => 'eng'
    ];
?>

<div class="lang">
    <ul>
        <?php foreach ($languages as $key => $language): ?>
            <?php if (app()->getLocale() == $key): ?>
                <li title="<?php echo $language; ?>">
                    <?php echo $language; ?><?php if ($key != 'ru'): ?><span>/</span><?php endif; ?>
                </li>
            <?php else: ?>
                <li title="<?php echo $language; ?>">
                    <a href="<?php echo url('/' . $key . $urlPath) ?>"><?php echo $language ?></a><?php if ($key != 'ru'): ?><span>/</span><?php endif; ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
    <div class="clearer"></div>
</div>