<?php

$path = '';

if (!isset($action)) {
    $action = 'showAbout';
}

if ($action == 'showAbout') {
    $path = 'about';
}

if ($action == 'showCreation') {
    $path = 'creation';
}

if ($action == 'showMultimedia') {
    $path = 'multimedia';
}

if ($action == 'showPress') {
    $path = 'press';
}

$pages = [
    'about',
    'creation',
    'multimedia',
    'press',
];

$lang = app()->getLocale();
?>

<div id="topMenu">
    <i class="sprite sprite-patterns_nav_left"></i>
    <div class="menu">
        <nav id="m_menu">
            <ul>
                <?php foreach ($pages as $page): ?>
                    <li title="<?php echo trans('common.' . $page); ?>" class="<?php if ($path == $page) echo "selectedLava"; ?>">
                        <a href="<?php echo url('/' . $lang . '/' . $page); ?>">
                            <span><?php echo trans('common.' . $page); ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
    <i class="sprite sprite-patterns_nav_right"></i>
</div>​
