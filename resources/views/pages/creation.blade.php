@extends('layout.master')

@section('content')

<div class="content">
    <div class="creation">

        <div class="creation-left">
            <div>
                <h3><?php echo $creations['romans']['name'][app()->getLocale()]; ?></h3>
                <ul>
                    <?php foreach ($creations['romans']['list'] as $key => $story): ?>
                        <li title="<?php echo $story['name'][app()->getLocale()] ?>" class="<?php echo !empty($story['files']) ? 'download' : '' ?>">
                            <p><?php echo $story['name'][app()->getLocale()] ?></p>
                            <span class="year"><?php echo $story['year'] ?></span>

                            <?php if (!empty($story['files'][app()->getLocale()])): ?>
                                <span class="downloaded_formats">
                                    <?php foreach ($story['files'][app()->getLocale()] as $format => $fileName): ?>
                                    <a href="<?php echo url('download/' . $fileName) ?>"><?php echo $format; ?></a>
                                    <?php endforeach; ?>
                                </span>
                            <?php endif; ?>

                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="creation-right">
            <div>
                <h3><?php echo $creations['fictions']['name'][app()->getLocale()]; ?></h3>
                <ul>
                    <?php foreach ($creations['fictions']['list'] as $key => $story): ?>
                        <li title="<?php echo $story['name'][app()->getLocale()] ?>" class="<?php echo !empty($story['files']) ? 'download' : '' ?>">
                            <p><?php echo $story['name'][app()->getLocale()] ?></p>
                            <span class="year"><?php echo $story['year'] ?></span>

                            <?php if (!empty($story['files'][app()->getLocale()])): ?>
                                <span class="downloaded_formats">
                                    <?php foreach ($story['files'][app()->getLocale()] as $format => $fileName): ?>
                                        <a href="<?php echo url('download/' . $fileName) ?>"><?php echo $format; ?></a>
                                    <?php endforeach; ?>
                                </span>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="clearer"></div>
    </div>

    <div class="creation">
        <h3><?php echo $creations['stories']['name'][app()->getLocale()]; ?></h3>

        <div class="creation-left">
            <ul>
                <?php foreach ($creations['stories']['list']['left'] as $key => $story): ?>
                    <li title="<?php echo $story['name'][app()->getLocale()] ?>" class="<?php echo !empty($story['files']) ? 'download' : '' ?>">
                        <p><?php echo $story['name'][app()->getLocale()] ?></p>
                        <span class="year"><?php echo $story['year'] ?></span>

                        <?php if (!empty($story['files'][app()->getLocale()])): ?>
                            <span class="downloaded_formats">
                                <?php foreach ($story['files'][app()->getLocale()] as $format => $fileName): ?>
                                    <a href="<?php echo url('download/' . $fileName) ?>"><?php echo $format; ?></a>
                                <?php endforeach; ?>
                            </span>
                        <?php endif; ?>
                    </li>

                <?php endforeach; ?>
            </ul>
        </div>

        <div class="creation-right">
            <ul>
                <?php foreach ($creations['stories']['list']['right'] as $key => $story): ?>
                    <li title="<?php echo $story['name'][app()->getLocale()] ?>" class="<?php echo !empty($story['files']) ? 'download' : '' ?>">
                        <p><?php echo $story['name'][app()->getLocale()] ?></p>
                        <span class="year"><?php echo $story['year'] ?></span>

                        <?php if (!empty($story['files'][app()->getLocale()])): ?>
                            <span class="downloaded_formats">
                                <?php foreach ($story['files'][app()->getLocale()] as $format => $fileName): ?>
                                    <a href="<?php echo url('download/' . $fileName) ?>"><?php echo $format; ?></a>
                                <?php endforeach; ?>
                            </span>
                        <?php endif; ?>

                    </li>

                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="clearer"></div>
</div>
@endsection