@extends('layout.master')

@section('content')
    <div class="content">
        <div class="press">
            <div>
                <article>
                    <span class="date">10 aprel, 2009</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/1'); ?>">
                            <i>"Hər gün ömürdən gedir"</i>
                        </a>
                    </h3>

                    <p>
                        İndinin yox, yaxın keçmişin gənclərinin çox sevdiyi bir yazıçı vardı. Məhəbbət, sevgi, həyat
                        dolu romanları, povestləri, hekayələri gənclərin əlindən, dilindən düşməzdi
                    </p>
                </article>

                <article>
                    <span class="date">yanvar, 2010</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/6'); ?>">
                            <i>Ürəklərdə yeri olan sənətkar.</i>
                        </a>
                    </h3>

                    <p>
                        Azərbaycan ədəbiyyatının görkəmli nümayəndələrindən biri olan Salam Qədirzadə ilə mənim ilk
                        tanışlığım 1977-ci ildə olub...
                    </p>
                </article>
                <div class="clearer"></div>
            </div>

            <div>
                <article>
                    <span class="date">2013</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/2'); ?>">
                            <i>"Məhəbbət Vətənə qalib gəldi!"</i>
                        </a>

                    </h3>

                    <p>
                        Kəndimizdə bir gözəl var - adı Leyladır onun "... Hər dəfə atamın - yazıçı Salam Qədirzadənin
                        qələminə məxsus bu
                        sətirlər yadıma düşəndə qəlbimi fərəh bürüyür....
                    </p>
                </article>

                <article>
                    <span class="date">10 апреля, 2013</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/4'); ?>">
                            <i>Салам Гадирзаде: история великого творчества и большой любви </i>
                        </a>
                    </h3>

                    <p>
                        Сегодня исполнилось 90 лет со дня рождения известного азербайджанского писателя и драматурга
                        Салама Гадирзаде...
                    </p>
                </article>
                <div class="clearer"></div>
            </div>

            <div>
                <article>
                    <span class="date">19 aprel, 2013</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/3'); ?>">
                            <i>"Qış gecəsi" nin yaz nəfəsi</i>
                        </a>
                    </h3>

                    <p>
                        Ötən əsrin 50-ci illərində ədəbiyyata gələn yazıçı Salam Qədirzadə o dövr oxucularının böyük
                        maraq göstərdiyi və sevdiyi nasirlərdən idi....
                    </p>
                </article>

                <article>
                    <span class="date">19 aprel, 2013</span>

                    <h3>
                        <a href="<?php echo url('/' . app()->getLocale() . '/press/show/5'); ?>">
                            <i>Sovet dövrünün populyar yazarlarından olan Salam Qədirzadənin oğlu ilə söhbət.</i>
                        </a>
                    </h3>

                    <p>
                        Hüsü Hacıyev küçəsində bir bina var: yazıçıların binası...
                    </p>
                </article>
                <div class="clearer"></div>
            </div>
        </div>
    </div>
@endsection