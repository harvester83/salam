@extends('layout.master')

@section('content')
    <div class="content content_about">
        <div class="collons">
            <div class="col-1">
                <p>
                    Салам Дадаш оглы Кадырзаде родился 10 апреля 1923 года в городе Баку.
                    С юных лет  он увлекался литературой, посещал различные кружки, пробовал писать рассказы и строил планы об учебе на фил.факе по окончании школы. Но Великая Отечественная война внесла свои коррективы в эти планы, отложив их ровно на 5 лет.
                </p>

                <p>
                    В 1941 году юный выпускник бакинской школы No.150 добровольцем отправился на фронт. В период с 1941 по 1945 годы в составе советской армии принимал участие в боях на Северном Кавказе, Кубани, Западной Украине, Польше, Бессарабии, Румынии, Чехословакии, Германии. За проявленные доблесть и отвагу был награжден Орденом Славы 3 степени, медалью «За Отвагу» и еще семью медалями и отличиями от Верховного Командование. Воспоминания об этом периоде жизни впоследствии нашли свое отражение в целом цикле произведений.
                </p>

                <p>
                    Демобилизовавшись из армии после окончания войны, С. Кадырзаде работает на должности заместителя начальника ПТУ No.18, а в 1948 году поступает на заочное отделение филологического факультета Азербайджанского Государственного Университета и одновременно работает ответственным секретарем местных передач Центрального радио.
                </p>

                <p>
                    В 1949 году по направлению Союза Писателей Азербайджана его направляют на учебу в Москву, в Лит.институт им. Горького. Первая же повесть юного писателя, написанная им в качестве дипломной работы, была выпущена отдельной книгой знаменитым московским издательством «Детгиз».
                    Здесь же в Москве Салам Кадырзаде женится и по завершении образования в 1952 году возвращается вместе с женой в Баку. На протяжении 8 лет он работает ответственным секретарем журнала «Азербайджан» - органа Союза Писателей Азербайджана.
                </p>
            </div>

            <div class="col-2">
                <p>
                    В 1960 году его назначают ответственным секретарем сатирического журнала «Кирпи», где он работает до 1966 года.
                    В период 1975-1976 годы Салам Кадырзаде является консультантом по драматургии Союза Писателей Азербайджана, а в 1976 году его назначают главным редактором журнала «Кирпи», где он работает до 1980 года.
                    На очередном Съезде Писателей Азербайджана, проводившемся при участии Гейдара Алиева, бывшего в тот период времени первым секретарем ЦК КП Азербайджана, Кадырзаде выбирают членом правления СП Азербайджана.
                </p>

                <p>
                    Салам Кадырзаде автор более 35 книг. Романы «Зимняя ночь», «46 фиалок», «У нас в селе есть красавица» и др. в 70-80-е годы были настоящими бестселлерами.  Читатели с нетерпением ждали от автора новых произведений, которые неизменно  печатались самыми большими тиражами.
                    Салам Кадырзаде известен и как драматург: его пьесы ставились не только в театрах республики, но и далеко за пределами Азербайджана, в том числе, в Украине, Молдове и др.  странах. Комедии «Где  ты, холостяцкая жизнь», «Хамиша-ханум», «Ширинбала набирает балы», «Гремящая любовь» и по сей день не теряют своей актуальности.
                </p>

                <p>
                    Салам Кадырзаде - Мастер юмора и сатиры. Его рассказы открыли целую страницу в истории азербайджанской литературы, заложив основы новой школы сатириков.
                </p>

                <p>
                    Салам Кадырзаде прожил творчески насыщенную, календарно короткую жизнь. Он умер 15 ноября 1987 года. Название его последней книги, которую ему так и не суждено было увидеть, удивительно символично - «Каждый день уходит из жизни».
                </p>
            </div>
            <div class="clearer"></div>
        </div>
    </div>
@endsection