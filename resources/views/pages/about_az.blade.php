@extends('layout.master')

@section('content')
    <div class="content content_about">
        <div class="collons">
            <div class="col-1">
                <p>
                    Salam Dadaş oğlu Qədirzadə 1923-cü ilin aprel ayının 10-da Bakı
                    şəhərində anadan olub. Hələ uşaq illərindən ədəbiyyata böyük həvəsi olan S.
                    Qədirzadə şeir və hekayələr yazır, ədəbi dərnəklərdə iştirak edir, məktəbdən
                    sonra məhz bu sahədə təhsil almağı planlaşdırırdı. Lakin Böyük Vətən müharibəsi
                    həmin arzuları düz 5 il təxirə saldı.
                </p>

                <p>
                    1941-ci ildə orta məktəbi bitirən kimi 18 yaşlı Salam könüllü şəkildə cəbhəyə yola düşür. 1941-1945-ci
                    illərdə o, Şimali Qafqaz, Kuban, Qərbi Ukrayna, Polşa, Bessarabiya, Rumıniya, Çexoslovakiya, eləcə də
                    Almaniyanın faşistlərdən azad edilməsində sıravi döyüşçü kimi iştirak edir, cəbhələrdə göstərdiyi şücaətə
                    görə III dərəcəli Şöhrət ordeni, İgidliyə görə və daha
                    yeddi medalla təltif olunmuş, Baş Ali Komandanlıq tərəfindən fərqləndirilmişdir.
                </p>

                <p>
                    1946-cı ildə ordu sıralarından tərxis olunduqdan sonra Salam Qədirzadə Sumqayıt şəhərində
                    18 saylı fabrik-zavod təhsili məktəbində müdir müavini vəzifəsinə təyin edilir. 1948-ci ildə
                    isə Azərbaycan Dövlət Universitetinin filologiya fakültəsinin qiyabi şöbəsinə daxil olur və eyni
                    zamanda Mərkəzi radionun yerli verilişləri üzrə məsul redaktoru vəzifəsində çalışmağa başlayır.
                </p>

                <p>
                    1949-cu ildə Azərbaycan Yazıçılar İttifaqının zəmanəti ilə Salam Qədirzadə Moskvadakı M.
                    Qorki adına Ədəbiyyat İnstitutuna təhsil almağa göndərilir. Onun hələ tələbə ikən diplom işi olaraq yazdığı
                    “Gənclik” povestini Rusiyanın məşhur “Detgiz” nəşriyyatı ayrıca kitab şəklində nəşr edir. Moskvada oxuyarkən
                    Salam ailə qurur və 1952-ci ildə təhsilini başa vurduqdan sonra həyat yoldaşı ilə birgə Vətənə qayıdır.
                    Burada gənc yazıçı 8 il Azərbaycan Yazıçılar İttifaqının orqanı olan “Azərbaycan”  jurnalının məsul katibi
                </p>
            </div>

            <div class="col-2">
                <p>
                    vəzifəsində işləyir. 1960- cı ildə S. Qədirzadə “Kirpi” satirik jurnalının məsul katibi təyin olunur
                    və 1966 ilədək həmin vəzifədə çalışır. 1975-1976 illərdə Salam Qədirzadə Yazıçılar İttifaqının
                    Dramaturgiya üzrə məsləhətçisi kimi çalışır. 1976-cı ildə isə “Kirpi” satirik jurnalının baş redaktoru
                    təyin olunur və 1980-cı ilədək həmin vəzifəni icra edir. Bir qədər sonra həmin dövrdə
                    Azərbaycan KP MK-nin birinci katiibi Heydər Əliyevin iştirakı
                    ilə keçirilən Azərbaycan yazıçılarının qurultayında Yazıçılar İttifaqının İdarə heyətinə üzv seçilir.
                </p>

                <p>
                    Salam Qədirzadə 35-dək kitab müəllifidir. Onun “Qış gecəsi”, “46 Bənövşə”, “Kəndimizdə bir gözəl var”, “
                    Sevdasız aylar” kimi romanları gənclərin masaüstü kitabına çevrilmişdir. Oxucular tərəfindən səbirsizliklə
                    gözlənilən əsərləri hər zaman ən yüksək tirajlarla nəşr olunurdu. Salam Qədirzadə dramaturq kimi də tamaşaçıların
                    rəğbətini qazanmış, onun pyesləri təkcə Azərbaycanda deyil, Ukrayna, Moldova və digər ölkələrdə səhnəyə qoyulmuşdur.
                    “Hardasan, ay subaylıq”,
                    “Həmişəxanım”, “Gurultulu məhəbbət”, “Şirinbala bal yığır” kimi komediyaları isə elə indi də gündəmdədir.
                </p>

                <p>
                    Salam Qədirzadə güclü satira və yumor ustasıdır. Onun satirik əsərləri müasir Azərbaycan ədəbiyyatında yeni bir
                    səhifə açdı, yeni məktəbin bünövrəsini qoydu. Salam Qədirzadə yaradıcılıq baxımından çox dolgun, təqvimi hesabla
                    isə qısa bir həyat yaşadı. O
                    , 1987-ci ilin noyabrın 15-də 64 yaşında dünyasını dəyişdi. Son kitabını “Hər gün ömürdən gedir” adlandırdı.
                </p>

                <p>
                    Bu kitabı görmək qismət olmadı ona...
                </p>
            </div>
            <div class="clearer"></div>
        </div>
    </div>
@endsection