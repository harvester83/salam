@extends('layout.master')

@section('content')

<div class="content">

    <?php foreach ($multimedia as $i => $section): ?>
        <div class="col-200 float-left <?php echo ($i != 0) ? 'pad-lt' : '' ?>">
            <?php foreach ($section as $photo): ?>
                <?php if ($photo['type'] == 'video'): ?>
                    <figure class="relative">
                        <a href="#"><img class="play_but" src="{{ URL::asset('img/play_but.png') }}" alt="<?php echo trans('common.title'); ?>"></a>
                        <a class="boxer" title="<?php echo trans('common.title'); ?>" href="<?php echo $photo['source']; ?>" data-gallery="gallery"><img src="{{ URL::asset('img/photos/' . $photo['icon'] . '.jpg') }}" alt="<?php echo trans('common.title'); ?>"></a>
                        <figcaption><?php echo $photo['title'][app()->getLocale()]; ?></figcaption>
                    </figure>
                <?php else: ?>
                    <figure>
                        <a class="boxer" title="<?php echo trans('common.title'); ?>" href="{{ URL::asset('img/multimedia/' . $photo['icon'] . '-lg.jpg') }}" data-gallery="gallery"><img src="{{ URL::asset('img/photos/' . $photo['icon'] . '.jpg') }}" alt="<?php echo trans('common.title'); ?>" class="example-image"></a>
                        <figcaption><?php echo $photo['title'][app()->getLocale()]; ?></figcaption>
                    </figure>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>

    <div class="clearer"></div>
</div>
@endsection